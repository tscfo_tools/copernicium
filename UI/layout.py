
import PySimpleGUI as sg
from countryinfo import CountryInfo # ISO 3166-Alpha3
country_list = list(country.title() for country in list(CountryInfo().all().keys()))

from tool_info import *


def main_layout():

    layout = [[sg.Column(
                [
                    [sg.Text("Reporting in:", justification='c')],
                    [sg.T("        "), sg.Button("Excel", key = "excel", size = (12, 1)), sg.T("     "), sg.Button("Google Sheets", key = "google_sheets", size = (12, 1)), sg.T("        ")]
                ], justification = 'c')],
                [sg.Column([[sg.Text("Help: User guide", tooltip = user_guide_link, enable_events=True, font=("Arial", 11, 'underline'), key = "URL " + user_guide_link)]], justification='c')]
            ]

    return layout


def select_action_layout():
    
    layout = [[sg.Column(
                [
                    [sg.Text("")],
                    [sg.T("        "), sg.Button("New client", key = "new_client"), sg.T("     "), sg.Button("Edit client", key = "edit_client"), sg.T("        ")]
                ], justification = 'c')],
                [sg.Column([[sg.Text("Help: User guide", tooltip = user_guide_link, enable_events=True, font=("Arial", 11, 'underline'), key = "URL " + user_guide_link)]], justification='c')]
            ]

    return layout


def new_layout():
    
    layout = [[sg.Column(
                [[sg.Text("       New client", font=15, tooltip = "Information about the new client")], 
                [sg.Text("           Client name", tooltip = "Name of the new client")],
                [sg.Text("         "), sg.InputText(key = "client_name", size = (58, 1), tooltip = "Name of the new client")],
                [sg.Text("           Regions", tooltip = "Name of the regions. Min: 0; Max: 3")],
                [sg.Text("         "), sg.InputText(key = "region_1", size = (18, 1), tooltip = "Name of the first region"), sg.InputText(key = "region_2", size = (18, 1), tooltip = "Name of the second region"), sg.InputText(key = "region_3", size = (18, 1), tooltip = "Name of the third region")],
                [sg.Text("           Companies", tooltip = "Client companies info: name, code and country (for assigning currency). Min: 0; Max: 4")],
                [sg.Text("           Company name"), sg.Text("                    Company code", tooltip = "Usually, 3 letters"), sg.Text("   Country", tooltip = "Select from the list")],
                [sg.Text("         "), sg.InputText(key = "company_name_1", size=(24, 1)), sg.InputText(key = "company_code_1", size=(12, 1)), sg.Combo(country_list, key="country_1", size=(16, 1))], # , default_value = "Spain"
                [sg.Text("         "), sg.InputText(key = "company_name_2", size=(24, 1)), sg.InputText(key = "company_code_2", size=(12, 1)), sg.Combo(country_list, key="country_2", size=(16, 1))],
                [sg.Text("         "), sg.InputText(key = "company_name_3", size=(24, 1)), sg.InputText(key = "company_code_3", size=(12, 1)), sg.Combo(country_list, key="country_3", size=(16, 1))],
                [sg.Text("         "), sg.InputText(key = "company_name_4", size=(24, 1)), sg.InputText(key = "company_code_4", size=(12, 1)), sg.Combo(country_list, key="country_4", size=(16, 1))],
                [sg.Text("           CCs", tooltip = "CCs for Personnel and Operating costs. Min:0; Max: 8")], 
                [sg.Text("           Personnel CCs"), sg.Text("                        "), sg.Text("Operating CCs")], 
                [sg.Text("         "), sg.InputText(key = "CC_P_1", default_text = "Operations", size=(22, 1)), sg.Text("         "), sg.InputText(key = "CC_O_1", default_text = "Operations", size=(22, 1))],
                [sg.Text("         "), sg.InputText(key = "CC_P_2", default_text = "Sales", size=(22, 1)), sg.Text("         "), sg.InputText(key = "CC_O_2", default_text = "Sales", size=(22, 1))],
                [sg.Text("         "), sg.InputText(key = "CC_P_3", default_text = "Marketing", size=(22, 1)), sg.Text("         "), sg.InputText(key = "CC_O_3", default_text = "Marketing", size=(22, 1))],
                [sg.Text("         "), sg.InputText(key = "CC_P_4", default_text = "R&D", size=(22, 1)), sg.Text("         "), sg.InputText(key = "CC_O_4", default_text = "R&D", size=(22, 1))],
                [sg.Text("         "), sg.InputText(key = "CC_P_5", default_text = "Admin", size=(22, 1)), sg.Text("         "), sg.InputText(key = "CC_O_5", default_text = "Admin", size=(22, 1))],
                [sg.Text("         "), sg.InputText(key = "CC_P_6", size=(22, 1)), sg.Text("         "), sg.InputText(key = "CC_O_6", size=(22, 1))],
                [sg.Text("         "), sg.InputText(key = "CC_P_7", size=(22, 1)), sg.Text("         "), sg.InputText(key = "CC_O_7", size=(22, 1))],
                [sg.Text("         "), sg.InputText(key = "CC_P_8", size=(22, 1)), sg.Text("         "), sg.InputText(key = "CC_O_8", size=(22, 1))],
                [sg.Text("       Format", font=15, tooltip = "Define main and secondary colors for cells interior, sheet tabs, titles font and graphs.")],
                [sg.Text("           Main color (HEX)", tooltip = "HEX color code"), sg.Text("                        "), sg.Text("Secondary color (HEX)", tooltip = "HEX color code")], 
                [sg.Text("         "), sg.InputText(key = "color_1", size=(22, 1)), sg.Text("         "), sg.InputText(key = "color_2", size=(22, 1))],
                [sg.T("")],
                [sg.Text("           Base model")], 
                [sg.Text("         "), sg.Input(size = (48, 1)), sg.FileBrowse(key = "model_path")],
                [sg.Text("           Output folder", tooltip = "Output Reporting name: 'YY<client_name>_Reporting_Forecast.xlsx'")], 
                [sg.Text("         "), sg.Input(size = (48, 1)), sg.FolderBrowse(key = "output_folder")],
                [sg.T("                        ",pad=(40,50,100,100)), sg.Button("Submit",key="submit"), sg.T("  "), sg.Button("Cancel")]
                ], size = (570, 580), scrollable = True)],
                [sg.Column([[sg.Text("Help: User guide", tooltip = user_guide_link, enable_events=True, font=("Arial", 11, 'underline'), key = "URL " + user_guide_link)]], justification='c')]
            ]

    return layout




def edit_main_layout_Copernicium():
    
    layout = [[sg.Column(
                [[sg.Text("       Edit client", font=15)], # [sg.T("   "), sg.Checkbox('CheckPL', default = False, key = "check_pl")]
                [sg.Text("           Reporting", tooltip = "Path of reporting to edit")], 
                [sg.Text("         "), sg.Input(size = (48, 1)), sg.FileBrowse(key = "reporting_path"), sg.Text("         ")],
                [sg.Text("           Edit options", tooltip = "Name of the new client")],
                [sg.Text("        "), sg.Checkbox('Regions', default = False, key = "edit_regions", tooltip = "Add or delete regions"), 
                 sg.Text("  "), sg.Checkbox('Companies', default = False, key = "edit_companies", tooltip = "Add or delete companies"), 
                 sg.Text("    "), sg.Checkbox('CCs', default = False, key = "edit_cc", tooltip = "Add or delete cost centers"), 
                 sg.Text("    "), sg.Checkbox('Formats', default = False, key = "edit_format", tooltip = "Edit cells interior color")],
                [sg.T("")],
                [sg.Text("                                           "), sg.Button("Submit",key="submit"), sg.T("  "), sg.Button("Cancel")]
                ])],
                [sg.Column([[sg.Text("Help: User guide", tooltip = user_guide_link, enable_events=True, font=("Arial", 11, 'underline'), key = "URL " + user_guide_link)]], justification='c')]
            ]

    return layout


def edit_main_layout_eCopernicium():
    
    layout = [[sg.Column(
                [[sg.Text("       Edit client", font=15)], # [sg.T("   "), sg.Checkbox('CheckPL', default = False, key = "check_pl")]
                [sg.Text("           Reporting", tooltip = "Url of reporting to edit")], 
                [sg.Text("         "), sg.InputText(key = "reporting_url", size = (56, 1)), sg.Text("         ")],
                [sg.Text("           Edit options", tooltip = "Name of the new client")],
                [sg.Text("        "), sg.Checkbox('Regions', default = False, key = "edit_regions", tooltip = "Add or delete regions"), 
                 sg.Text("  "), sg.Checkbox('Companies', default = False, key = "edit_companies", tooltip = "Add or delete companies"), 
                 sg.Text("    "), sg.Checkbox('CCs', default = False, key = "edit_cc", tooltip = "Add or delete cost centers"), 
                 sg.Text("    "), sg.Checkbox('Formats', default = False, key = "edit_format", tooltip = "Edit cells interior color")],
                [sg.T("")],
                [sg.Text("                                           "), sg.Button("Submit",key="submit"), sg.T("  "), sg.Button("Cancel")]
                ])],
                [sg.Column([[sg.Text("Help: User guide", tooltip = user_guide_link, enable_events=True, font=("Arial", 11, 'underline'), key = "URL " + user_guide_link)]], justification='c')]
            ]

    return layout



def edit_secondary_layout(user_inputs):

    main_body = []
    # Add header
    if user_inputs.edit_regions or user_inputs.edit_companies or user_inputs.edit_cc:
        main_body = main_body + [[sg.Text("       Edit client", font=15, tooltip = "Information about the new client")]]
        
    # Add regions section
    if user_inputs.edit_regions:
        main_body = main_body + [[sg.Text("           Regions", tooltip = "Name of the regions. Min: 0; Max: 3")],
                [sg.Text("        "), 
                 sg.InputText(key = "region_1", size = (16, 1), default_text = user_inputs.regions[0], tooltip = "Name of the first region"), 
                 sg.InputText(key = "region_2", size = (16, 1), default_text = user_inputs.regions[1], tooltip = "Name of the second region"), 
                 sg.InputText(key = "region_3", size = (16, 1), default_text = user_inputs.regions[2],  tooltip = "Name of the third region"),
                 sg.Text("         ")
                ]]
    
    # Add companies section
    if user_inputs.edit_companies:
        main_body = main_body + [[sg.Text("           Companies", tooltip = "Client companies info: name, code and country (for assigning currency). Min: 0; Max: 4.")],
                [sg.Text("           Company name"), sg.Text("                Company code", tooltip = "Usually, 3 letters"), sg.Text("Country", tooltip = "Country is not available in Reporting\nPlease, select country again.")],
                [sg.Text("        "), sg.InputText(key = "company_name_1", size=(22, 1), default_text = user_inputs.comp_name[0]), sg.InputText(key = "company_code_1", size=(12, 1), default_text = user_inputs.comp_code[0]), sg.Combo(country_list, key="country_1", size=(13, 1)), sg.Text("        ")],
                [sg.Text("        "), sg.InputText(key = "company_name_2", size=(22, 1), default_text = user_inputs.comp_name[1]), sg.InputText(key = "company_code_2", size=(12, 1), default_text = user_inputs.comp_code[1]), sg.Combo(country_list, key="country_2", size=(13, 1))],
                [sg.Text("        "), sg.InputText(key = "company_name_3", size=(22, 1), default_text = user_inputs.comp_name[2]), sg.InputText(key = "company_code_3", size=(12, 1), default_text = user_inputs.comp_code[2]), sg.Combo(country_list, key="country_3", size=(13, 1))],
                [sg.Text("        "), sg.InputText(key = "company_name_4", size=(22, 1), default_text = user_inputs.comp_name[3]), sg.InputText(key = "company_code_4", size=(12, 1), default_text = user_inputs.comp_code[3]), sg.Combo(country_list, key="country_4", size=(13, 1))],
                ]

    # Add CCs section
    if user_inputs.edit_cc:
        main_body = main_body + [[sg.Text("           CCs", tooltip = "CCs for Personnel and Operating costs. Min:0; Max: 8")], 
                [sg.Text("           Personnel CCs"), sg.Text("                        "), sg.Text("Operating CCs")], 
                [sg.Text("        "), sg.InputText(key = "CC_P_1", default_text = user_inputs.CC_P[0], size=(22, 1)), sg.Text("        "), sg.InputText(key = "CC_O_1", default_text = user_inputs.CC_O[0], size=(22, 1)), sg.Text("        ")],
                [sg.Text("        "), sg.InputText(key = "CC_P_2", default_text = user_inputs.CC_P[1], size=(22, 1)), sg.Text("        "), sg.InputText(key = "CC_O_2", default_text = user_inputs.CC_O[1], size=(22, 1))],
                [sg.Text("        "), sg.InputText(key = "CC_P_3", default_text = user_inputs.CC_P[2], size=(22, 1)), sg.Text("        "), sg.InputText(key = "CC_O_3", default_text = user_inputs.CC_O[2], size=(22, 1))],
                [sg.Text("        "), sg.InputText(key = "CC_P_4", default_text = user_inputs.CC_P[3], size=(22, 1)), sg.Text("        "), sg.InputText(key = "CC_O_4", default_text = user_inputs.CC_O[3], size=(22, 1))],
                [sg.Text("        "), sg.InputText(key = "CC_P_5", default_text = user_inputs.CC_P[4], size=(22, 1)), sg.Text("        "), sg.InputText(key = "CC_O_5", default_text = user_inputs.CC_O[4], size=(22, 1))],
                [sg.Text("        "), sg.InputText(key = "CC_P_6", default_text = user_inputs.CC_P[5], size=(22, 1)), sg.Text("        "), sg.InputText(key = "CC_O_6", default_text = user_inputs.CC_O[5], size=(22, 1))],
                [sg.Text("        "), sg.InputText(key = "CC_P_7", default_text = user_inputs.CC_P[6], size=(22, 1)), sg.Text("        "), sg.InputText(key = "CC_O_7", default_text = user_inputs.CC_O[6], size=(22, 1))],
                [sg.Text("        "), sg.InputText(key = "CC_P_8", default_text = user_inputs.CC_P[7], size=(22, 1)), sg.Text("        "), sg.InputText(key = "CC_O_8", default_text = user_inputs.CC_O[7], size=(22, 1))]
                ]

    # Add Format section
    if user_inputs.edit_format:
        main_body = main_body + [
                [sg.Text("")],
                [sg.Text("       Format", font=15, tooltip = "Define main and secondary colors for cells interior, sheet tabs, titles font and graphs.")],
                [sg.Text("                               Main color (HEX)", tooltip = "Previous main color = interior cell color 'REPORTING->'!B5"), sg.Text("      "), sg.Text("Secondary color (HEX)", tooltip = "Previous secondary color = interior cell color 'REPORTING->'!B6")], 
                [sg.Text("       "), sg.Text("Previous:   "), sg.InputText(key = "old_color_1", size=(20, 1), default_text = user_inputs.color_1_old), sg.InputText(key = "old_color_2", size=(20, 1), default_text = user_inputs.color_2_old), sg.Text("        ")],
                [sg.Text("       "), sg.Text("New:         "), sg.InputText(key = "new_color_1", size=(20, 1)), sg.InputText(key = "new_color_2", size=(20, 1))]
                ]



    layout = main_body + \
            [
             [sg.T("")],
             [sg.T("                                      "), sg.Button("Submit",key="submit"), sg.T("  "), sg.Button("Cancel")],
             [sg.Column([[sg.Text("Help: User guide", tooltip = user_guide_link, enable_events=True, font=("Arial", 11, 'underline'), key = "URL " + user_guide_link)]], justification='c')]
            ]

    return layout
