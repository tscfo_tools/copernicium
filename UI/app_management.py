

import PySimpleGUI as sg
from webbrowser import open as open_browser
from .layout import main_layout

from eCopernicium.UI.app_management import open_app as run_eCopernicium, actions_main_window as eCopernicium_actions
from Copernicium.UI.app_management import open_app as run_Copernicium, actions_main_window as Copernicium_actions
from tool_info import *


# Define theme for all windows
sg.theme('DefaultNoMoreNagging') # ("Default") #  ("LightGrey1") # ("SystemDefaultForReal") # 
sg.set_options(font=('Helvetica', 10))


def open_app(app_name, icon_path):
    
    layout = main_layout()
    main_win = sg.Window(app_name, layout, icon=icon_path, auto_size_text=18, resizable=True, finalize=True)
    main_win.normal()
    
    return main_win


def actions_main_window(main_win):
    
    while True:
        event, values = main_win.read()

        if event == sg.WIN_CLOSED:
            break
        elif event == "Cancel":
            main_win.close()
            break

        elif event.startswith("URL "):
            url = event.split(' ')[1]
            open_browser(url)

        elif event == "google_sheets":
            # Run eCopernicium (for Google Sheets)
            eCopernicium_win = run_eCopernicium("eCopernicium", tool_cover_link)
            # App responses
            eCopernicium_actions(eCopernicium_win)


        elif event == "excel":
            # Run Copernicium (for Excel)
            Copernicium_win = run_Copernicium("Copernicium", tool_cover_link)
            # App responses
            Copernicium_actions(Copernicium_win)


    return
