
#######################################################
# Run batchUpdate requests
#######################################################


def run_batchUpdate_request(service, spreadsheet_id, request_body_json):
    try:    
        response = service.spreadsheets().batchUpdate(
            spreadsheetId=spreadsheet_id,
            body=request_body_json,
        ).execute()
        return response
    except Exception as e:
        print(e)
        return None