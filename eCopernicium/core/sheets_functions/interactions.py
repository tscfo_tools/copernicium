
from eCopernicium.core.sheets_functions.requests import *

#######################################################
# Insert rows
#######################################################


def insert_rows(service, spreadsheet_id, sheet_id, ini_row, end_row):
    request_body = {
        'requests': [
            {
                'insertDimension': {
                    'range': {
                        'sheetId': sheet_id,
                        'dimension': 'ROWS',
                        'startIndex': ini_row - 1,
                        'endIndex': end_row - 1
                    }
                }
            }
        ]
    }
    
    run_batchUpdate_request(service, spreadsheet_id, request_body)
    
    return