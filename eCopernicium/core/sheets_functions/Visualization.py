# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 16:53:53 2022

@author: Alberto Cañizares
"""

from .spreadsheets_and_worksheets import get_sheet_index
from .requests import run_batchUpdate_request

#######################################################
# Hide/ Unhide columns, rows and sheets
#######################################################

def apply_hide(service, spreadsheet_id, sheet_id, ini_ind, end_ind, request_body = None, dim = "COLUMNS"):
    
    rq = {
        'updateDimensionProperties': {
            "range": {
                "sheetId": sheet_id,
                "dimension": dim,
                "startIndex": ini_ind,
                "endIndex": end_ind,
            },
            "properties": {
                "hiddenByUser": True,
            },
            "fields": 'hiddenByUser'
        }
    }
    
    if request_body == None:
        request_body = {'requests': [rq]}   
        run_batchUpdate_request(service, spreadsheet_id, request_body)
        return None
    else:
        request_body['requests'].append(rq)
        return request_body


def apply_unhide(service, spreadsheet_id, sheet_id, ini_ind = None, end_ind = None, request_body = None, dim = "COLUMNS"):
    
    
    if ini_ind == None or end_ind == None:
        rq = {
            'updateDimensionProperties': {
                "range": {
                    "sheetId": sheet_id,
                    "dimension": dim
                },
                "properties": {
                    "hiddenByUser": False,
                },
                "fields": 'hiddenByUser'
            }
        }
    else:
        rq = {
            'updateDimensionProperties': {
                "range": {
                    "sheetId": sheet_id,
                    "dimension": dim,
                    "startIndex": ini_ind,
                    "endIndex": end_ind
                },
                "properties": {
                    "hiddenByUser": False,
                },
                "fields": 'hiddenByUser'
            }
        }
    
    if request_body == None:
        request_body = {'requests': [rq]}   
        run_batchUpdate_request(service, spreadsheet_id, request_body)
        return None
    else:
        request_body['requests'].append(rq)
        return request_body


   
def hide_sheet(service, spreadsheet_id, sheet_name, sheet_id, sheet_index, rowCount = 1000, columnCount = 100, tab_color = None, request_body = None):   
    
    if tab_color != None:
        rq = {
            'updateSheetProperties': {
                'properties': {
                    'sheetId': sheet_id,
                    'title': sheet_name,
                    'index': sheet_index,
                    'tabColorStyle': {
                        'rgbColor': tab_color
                    },
                    'hidden': True,
                    'gridProperties': {
                        'rowCount': rowCount,
                        'columnCount': columnCount,
                        'hideGridlines': True
                        }
                },
                'fields': '*'
            }
        }
    else:
        rq = {
            'updateSheetProperties': {
                'properties': {
                    'sheetId': sheet_id,
                    'title': sheet_name,
                    'index': sheet_index,
                    'hidden': True,
                    'gridProperties': {
                        'rowCount': rowCount,
                        'columnCount': columnCount,
                        'hideGridlines': True
                        }
                },
                'fields': '*'
            }
        }

    if request_body == None:
        request_body = {'requests': [rq]}   
        run_batchUpdate_request(service, spreadsheet_id, request_body)
        return None
    else:
        request_body['requests'].append(rq)
        return request_body




#######################################################
# Gruping/ Ungrouping
#######################################################

def create_group(service, spreadsheet_id, sheet_id, ini_index, end_index, request_body = None, collapsed = True, dim = "COLUMNS"):  
        
    rq = {
        "addDimensionGroup": {
            "range": {
                "dimension": dim,
                "sheetId": sheet_id,
                "startIndex": ini_index,
                "endIndex": end_index
            }
        }
    }

    if collapsed:
        rq = {
            "updateDimensionGroup": {
                "dimensionGroup": {
                    "range": {
                        "dimension": dim,
                        "sheetId": sheet_id,
                        "startIndex": ini_index,
                        "endIndex": end_index
                    },
                    "depth": 1,
                    "collapsed": True
                },
                "fields": "*"
            }
        }
        
    if request_body == None:
        request_body = {'requests': [rq]}   
        run_batchUpdate_request(service, spreadsheet_id, request_body)
        return None
    else:
        request_body['requests'].append(rq)
        return request_body



def delete_group(service, spreadsheet_id, sheet_id, request_body = None, dim = "COLUMNS"):  
        
    rq = {
        "deleteDimensionGroup": {
            "range": {
                "dimension": dim,
                "sheetId": sheet_id
            }
        }
    }
    
    if request_body == None:
        request_body = {'requests': [rq]}   
        # run_batchUpdate_request(service, spreadsheet_id, request_body)
    else:
        request_body['requests'].append(rq)

    # unhide if it is still hidden
    request_body = apply_unhide(service, spreadsheet_id, sheet_id, request_body = request_body)

    if request_body == None:
        request_body = {'requests': [rq]}
        request_body = apply_unhide(service, spreadsheet_id, sheet_id, request_body = request_body)
        run_batchUpdate_request(service, spreadsheet_id, request_body)
        return None
    else:
        request_body['requests'].append(rq)
        request_body = apply_unhide(service, spreadsheet_id, sheet_id, request_body = request_body)
        return request_body



def collapse_all_col_groups(service, spreadsheet_id, sheet_name):
    spreadsheet = service.spreadsheets().get(
        spreadsheetId=spreadsheet_id,
        ranges=sheet_name,
        includeGridData=True
    ).execute()
    
    grouping_data = spreadsheet['sheets'][0]["columnGroups"]
    # Initialize request
    request_body = {'requests': []}
    for i in range(len(grouping_data)):
        rq = {
            "updateDimensionGroup": {
                "dimensionGroup": {
                    "range": grouping_data[i]['range'],
                    "depth": grouping_data[i]['depth'],
                    "collapsed": True
                },
                "fields": "*"
            }
        }
        request_body['requests'].append(rq)
        
    run_batchUpdate_request(service, spreadsheet_id, request_body)
    
    return



def uncollapse_all_col_groups(service, spreadsheet_id, sheet_name):
    spreadsheet = service.spreadsheets().get(
        spreadsheetId=spreadsheet_id,
        ranges=sheet_name,
        includeGridData=True
    ).execute()
    
    grouping_data = spreadsheet['sheets'][0]["columnGroups"]
    for i in range(len(grouping_data)):
        request_body = {
            "requests": [
                {
                    "updateDimensionGroup": {
                        "dimensionGroup": {
                            "range": grouping_data[i]['range'],
                            "depth": grouping_data[i]['depth'],
                            "collapsed": False
                        },
                        "fields": "*"
                    }
                }
            ]
        }
        
        run_batchUpdate_request(service, spreadsheet_id, request_body) 
    
    return

#######################################################
# Autofit
#######################################################


def autofit(service, spreadsheet_id, sheet_id, request_body = None, dim = "COLUMNS", ini_index = None, end_index = None):
    
    if ini_index != None and end_index != None:
        rq = {
            'autoResizeDimensions': {
                'dimensions': {
                    'sheetId': sheet_id,
                    'dimension': dim,
                    'startIndex': ini_index, 
                    'endIndex': end_index
                }
            }
        }
    else:
        rq = {
            'autoResizeDimensions': {
                'dimensions': {
                    'sheetId': sheet_id,
                    'dimension': dim
                }
            }
        }
    
    if request_body == None:
        request_body = {'requests': [rq]}   
        run_batchUpdate_request(service, spreadsheet_id, request_body)
        return None
    else:
        request_body['requests'].append(rq)
        return request_body



#######################################################
# Show/Hide gridlines
#######################################################


def show_gridline(service, spreadsheet_id, sheet_id, sheet_name, request_body = None, hide_gridline=True):

    ref_sheet_index = get_sheet_index(spreadsheet_id, [sheet_name])[0]
    rq = {
        'updateSheetProperties': {
            'properties': {
                'sheetId': sheet_id,
                'title': sheet_name,
                'index': ref_sheet_index,
                'gridProperties': {
                    'rowCount': 1000,
                    'columnCount': 500,
                    'hideGridlines': hide_gridline
                    }
            },
            'fields': '*'
        }
    }

    if request_body == None:
        request_body = {'requests': [rq]}   
        run_batchUpdate_request(service, spreadsheet_id, request_body)
        return None
    else:
        request_body['requests'].append(rq)
        return request_body
