
from .requests import run_batchUpdate_request


#######################################################
# Copy format
#######################################################


def copy_format(service, spreadsheet_id, sheet_id, src_range, dst_range, request_body = None, paste_type = 'PASTE_FORMAT'):
    """
    
    Parameters
    ----------
    service :
    spreadsheet_id : 
    sheet_id :
    src_range : range to copy from
        [[ini_row, end_row], [ini_col, end_col]]
    dst_range : range to paste
        [[ini_row, end_row], [ini_col, end_col]]

    Returns
    -------
    None.

    """
    rq = {
        'copyPaste': {
            'source': {
                    'sheetId': sheet_id,
                    'startRowIndex': src_range[0][0] - 1,
                    'endRowIndex': (src_range[0][1] - 1) + 1,
                    'startColumnIndex': src_range[1][0] - 1,
                    'endColumnIndex': src_range[1][1] - 1
            },
            'destination': {
                    'sheetId': sheet_id,
                    'startRowIndex': dst_range[0][0] - 1,
                    'endRowIndex': (dst_range[0][1] - 1) + 1,
                    'startColumnIndex': dst_range[1][0] - 1,
                    'endColumnIndex': (dst_range[1][1] - 1) + 1
            },
            'pasteType': paste_type
        }
    }
             
    if request_body == None:
        request_body = {'requests': [rq]}   
        run_batchUpdate_request(service, spreadsheet_id, request_body)
        return None
    else:
        request_body['requests'].append(rq)
        return request_body



#######################################################
# Formatting Numbers 
#######################################################

def formatting_number(service, spreadsheet_id, sheet_id, range_to_format, request_body = None, number_type = 'NUMBER', number_pattern = '#,###.00'):

    rq = {
        'repeatCell': {
            'range': {
                'sheetId': sheet_id,
                'startRowIndex': range_to_format[0][0] - 1,
                'endRowIndex': (range_to_format[1][0] - 1) + 1,
                'startColumnIndex': range_to_format[0][1] - 1,
                'endColumnIndex': (range_to_format[1][1] - 1) + 1
            },
            'cell': {
                'userEnteredFormat': {
                    'numberFormat': {
                        'type': number_type,
                        'pattern': number_pattern
                    }
                }
            },
            'fields': 'userEnteredFormat(numberFormat)'
        }
    }

    if request_body == None:
        request_body = {'requests': [rq]}   
        run_batchUpdate_request(service, spreadsheet_id, request_body)
        return None
    else:
        request_body['requests'].append(rq)
        return request_body



#######################################################
# Formatting Text color 
#######################################################

def formatting_text_color(service, spreadsheet_id, sheet_id, range_to_format, request_body = None, rgb_pattern=[0, 0, 0], bold_op=False, fontsize = None):

    rq = {
        'repeatCell': {
            'range': {
                'sheetId': sheet_id,
                'startRowIndex': range_to_format[0][0] - 1,
                'endRowIndex': (range_to_format[1][0] - 1) + 1,
                'startColumnIndex': range_to_format[0][1] - 1,
                'endColumnIndex': (range_to_format[1][1] - 1) + 1
            },
            'cell': {
                'userEnteredFormat': {
                    'textFormat': {
                        'foregroundColor': {
                            'red': rgb_pattern[0],
                            'green': rgb_pattern[1],
                            'blue': rgb_pattern[2],
                        },
                        'bold': bold_op
                    }
                }
            },
            'fields': 'userEnteredFormat(textFormat)'
        }
    }
    if fontsize != None: rq['repeatCell']['cell']['userEnteredFormat']['textFormat']['fontSize'] = fontsize
    
    if request_body == None:
        request_body = {'requests': [rq]}   
        run_batchUpdate_request(service, spreadsheet_id, request_body)
        return None
    else:
        request_body['requests'].append(rq)
        return request_body



#######################################################
# Formatting Background color
#######################################################

def formatting_background_color(service, spreadsheet_id, sheet_id, range_to_format, request_body = None, rgb_pattern=[0, 0, 0]):

    rq = {
        'repeatCell': {
            'range': {
                'sheetId': sheet_id,
                'startRowIndex': range_to_format[0][0] - 1,
                'endRowIndex': (range_to_format[1][0] - 1) + 1,
                'startColumnIndex': range_to_format[0][1] - 1,
                'endColumnIndex': (range_to_format[1][1] - 1) + 1
            },
            'cell': {
                'userEnteredFormat': {
                    'backgroundColor': {
                        'red': rgb_pattern[0],
                        'green': rgb_pattern[1],
                        'blue': rgb_pattern[2]
                    }
                }
            },
            'fields': 'userEnteredFormat(backgroundColor)'
        }
    }

    if request_body == None:
        request_body = {'requests': [rq]}   
        run_batchUpdate_request(service, spreadsheet_id, request_body)
        return None
    else:
        request_body['requests'].append(rq)
        return request_body



#######################################################
# Formatting Borders
#######################################################

def formatting_borders(service, spreadsheet_id, sheet_id, range_to_format, request_body = None, border_loc=['top', 'bottom', 'left', 'right'], border_style=['SOLID', 'SOLID', 'SOLID', 'SOLID']):

    # border_style = 'DOTTED', 'DASHED', 'SOLID', 'SOLID_MEDIUM', 'SOLID_THICK', 'NONE', 'DOUBLE'
    rq = {
        'updateBorders': {
            'range': {
                'sheetId': sheet_id,
                'startRowIndex': range_to_format[0][0] - 1,
                'endRowIndex': (range_to_format[1][0] - 1) + 1,
                'startColumnIndex': range_to_format[0][1] - 1,
                'endColumnIndex': (range_to_format[1][1] - 1) + 1
            }
        }
    }

    for i in range(len(border_loc)):
        rq['updateBorders'][border_loc[i]] = {
              "style": border_style[i],
              "color": {
                  'red': 0,
                  'green': 0,
                  'blue': 0
              },
            }

    if request_body == None:
        request_body = {'requests': [rq]}   
        run_batchUpdate_request(service, spreadsheet_id, request_body)
        return None
    else:
        request_body['requests'].append(rq)
        return request_body


#######################################################
# Apply Bold
#######################################################

def apply_bold(service, spreadsheet_id, sheet_id, range_to_format, request_body = None, bold=True):

    rq = {
        'repeatCell': {
            'range': {
                'sheetId': sheet_id,
                'startRowIndex': range_to_format[0][0] - 1,
                'endRowIndex': (range_to_format[1][0] - 1) + 1,
                'startColumnIndex': range_to_format[0][1] - 1,
                'endColumnIndex': (range_to_format[1][1] - 1) + 1
            },
            'cell': {
                'userEnteredFormat': {
                    'textFormat': {
                        'bold': True
                    }
                }
            },
            'fields': 'userEnteredFormat(textFormat)'
        }
    }

    if request_body == None:
        request_body = {'requests': [rq]}   
        run_batchUpdate_request(service, spreadsheet_id, request_body)
        return None
    else:
        request_body['requests'].append(rq)
        return request_body

#######################################################
# Modify charts background color
#######################################################


def update_chart_bg_color(service, spreadsheet_id, chart_id, chart_spec_rq, bg_color = [0, 0, 0], request_body = None):

    chart_spec_rq['backgroundColorStyle']['rgbColor'] = {'red': bg_color[0], 'green': bg_color[1], 'blue': bg_color[2]}
    
    rq = {
        'updateChartSpec' : {
            "chartId": chart_id,
            "spec": chart_spec_rq 
        }
    }

    if request_body == None:
        request_body = {'requests': [rq]}
        run_batchUpdate_request(service, spreadsheet_id, request_body)
        return None
    else:
        request_body['requests'].append(rq)
        return request_body