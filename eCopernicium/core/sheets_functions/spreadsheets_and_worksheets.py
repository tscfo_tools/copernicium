# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 16:58:08 2022

@author: Alberto Cañizares
"""

import os 
from .google_apis import Create_Service
from eCopernicium.client_secret_account_json import *
from .requests import run_batchUpdate_request

def get_spreadsheetId(gsheet_url):
    return gsheet_url[gsheet_url.find('/d/')+3:gsheet_url.find('/d/')+3 + gsheet_url[gsheet_url.find('/d/')+3:].find('/')]

def get_sheetId(spreadsheet_id, sheet_names):
    API_NAME = 'sheets'
    API_VERSION = 'v4'
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
    
    service = Create_Service(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES)
    
    spreadsheet = service.spreadsheets().get(
        spreadsheetId=spreadsheet_id
    ).execute()
    
    sheet_ids = []
    for sheet_name in sheet_names:
        for _sheet in spreadsheet['sheets']:
            if _sheet['properties']['title'] == sheet_name:
                sheet_ids.append(_sheet['properties']['sheetId'])
    
    if len(sheet_ids) > 0:
        return sheet_ids
    else:
        return None

def get_sheet_name(service, spreadsheet_id, sheet_ids):
    
    spreadsheet = service.spreadsheets().get(
        spreadsheetId=spreadsheet_id
    ).execute()
    
    sheet_names = []
    for sheet_id in sheet_ids:
        for _sheet in spreadsheet['sheets']:
            if _sheet['properties']['sheetId'] == sheet_id:
                sheet_names.append(_sheet['properties']['title'])
    
    if len(sheet_names) == 1:
        return sheet_names[0]
    elif len(sheet_names) > 1:
        return sheet_names
    else:
        return None



def get_sheet_index(spreadsheet_id, sheet_names, CLIENT_SECRET_FILE = '.config/client_secret_account.json'):
    API_NAME = 'sheets'
    API_VERSION = 'v4'
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
    
    service = Create_Service(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES)
    
    spreadsheet = service.spreadsheets().get(
        spreadsheetId=spreadsheet_id
    ).execute()
    
    sheet_ids = []
    for sheet_name in sheet_names:
        for _sheet in spreadsheet['sheets']:
            if _sheet['properties']['title'] == sheet_name:
                sheet_ids.append(_sheet['properties']['index'])
    
    if len(sheet_ids) > 0:
        return sheet_ids
    else:
        return None


def add_worksheet(service, spreadsheet_id, new_sheet_name, after_sheet = None):
    
    if after_sheet == None:
        request_body = {
            'requests': [{
                'addSheet': {
                    'properties': {
                        'title': new_sheet_name
                    }    
                }    
            }]
        }
        
    else:
        ref_sheet_index = get_sheet_index(spreadsheet_id, [after_sheet])[0]
        request_body = {
            'requests': [{
                'addSheet': {
                    'properties': {
                        'title': new_sheet_name,
                        'index': ref_sheet_index + 1
                    }    
                }    
            }]
        }
    
    run_batchUpdate_request(service, spreadsheet_id, request_body)
    
    return




def get_worksheet_list_from_spreadsheet(service, spreadsheet_id):
    
    ss = service.spreadsheets().get(
        spreadsheetId=spreadsheet_id
    ).execute()
    
    ws_list = []
    for ws in ss['sheets']:
        ws_list.append(ws['properties']['title'])
        
    return ws_list
    
def get_locale(service, spreadsheet_id):
    ss = service.spreadsheets().get(
            spreadsheetId=spreadsheet_id
        ).execute()
    
    return ss['properties']['locale']

    

def get_named_range_info(service, spreadsheet_id, sheet_name):
    
    sheet_info = service.spreadsheets().get(
                spreadsheetId=spreadsheet_id,
                ranges=sheet_name,
                includeGridData=True
            ).execute()  
    
    named_range_info = sheet_info['namedRanges']
    custom_named_range_info = dict()
    for i in range(len(named_range_info)):
        custom_named_range_info[named_range_info[i]['name']] = named_range_info[i]['range']
    
    return custom_named_range_info



def get_sheets_info(service, spreadsheet_id):

    spreadsheet = service.spreadsheets().get(
            spreadsheetId=spreadsheet_id
        ).execute()
    
    sheet_info = spreadsheet['sheets']
    custom_sheet_info = dict()
    for i in range(len(sheet_info)):
        custom_sheet_info[sheet_info[i]['properties']['title']] = dict()
        custom_sheet_info[sheet_info[i]['properties']['title']]['sheetId'] = sheet_info[i]['properties']['sheetId']
        custom_sheet_info[sheet_info[i]['properties']['title']]['index'] = sheet_info[i]['properties']['index']
        custom_sheet_info[sheet_info[i]['properties']['title']]['rows'] = sheet_info[i]['properties']['gridProperties']['rowCount']
        custom_sheet_info[sheet_info[i]['properties']['title']]['cols'] = sheet_info[i]['properties']['gridProperties']['columnCount']
        try:
            custom_sheet_info[sheet_info[i]['properties']['title']]['tab_color'] = sheet_info[i]['properties']['tabColorStyle']['rgbColor']
        except: pass
    
    return custom_sheet_info


def rename_sheet(service, spreadsheet_id, new_sheet_name, old_sheet_name, sheet_id, sheet_index, rowCount = 1000, columnCount = 100, tab_color = None, request_body = None):   
    
    if tab_color != None:
        rq = {
            'updateSheetProperties': {
                'properties': {
                    'sheetId': sheet_id,
                    'title': new_sheet_name,
                    'index': sheet_index,
                    'tabColorStyle': {
                        'rgbColor': tab_color
                    },
                    'hidden': False,
                    'gridProperties': {
                        'rowCount': rowCount,
                        'columnCount': columnCount,
                        'hideGridlines': True
                        }
                },
                'fields': '*'
            }
        }    
    else:
        rq = {
            'updateSheetProperties': {
                'properties': {
                    'sheetId': sheet_id,
                    'title': new_sheet_name,
                    'index': sheet_index,
                    'hidden': False,
                    'gridProperties': {
                        'rowCount': rowCount,
                        'columnCount': columnCount,
                        'hideGridlines': True
                        }
                },
                'fields': '*'
            }
        }

    if request_body == None:
        request_body = {'requests': [rq]}   
        run_batchUpdate_request(service, spreadsheet_id, request_body)
        return None
    else:
        request_body['requests'].append(rq)
        return request_body


def define_sheet_props(service, spreadsheet_id, sheet_name, sheet_id, sheet_index, rowCount=1000, columnCount=100, tab_color=None, request_body=None):

    if tab_color != None:
        rq = {
            'updateSheetProperties': {
                'properties': {
                    'sheetId': sheet_id,
                    'title': sheet_name,
                    'index': sheet_index,
                    'tabColorStyle': {
                        'rgbColor': {
                                'red': tab_color[0],
                                'green': tab_color[1],
                                'blue': tab_color[2]
                            }
                    },
                    'gridProperties': {
                        'rowCount': rowCount,
                        'columnCount': columnCount,
                        'hideGridlines': True
                    }
                },
                'fields': '*'
            }
        }
    else:
        rq = {
            'updateSheetProperties': {
                'properties': {
                    'sheetId': sheet_id,
                    'title': sheet_name,
                    'index': sheet_index,
                    'gridProperties': {
                        'rowCount': rowCount,
                        'columnCount': columnCount,
                        'hideGridlines': True
                    }
                },
                'fields': '*'
            }
        }

    if request_body == None:
        request_body = {'requests': [rq]}
        run_batchUpdate_request(service, spreadsheet_id, request_body)
        return None
    else:
        request_body['requests'].append(rq)
        return request_body
