
from .requests import run_batchUpdate_request

#######################################################
# Read data
#######################################################

def read_ws(service, spreadsheet_id, sheet_name, read_dim = 'ROWS'):
    try:
        spreadsheet_data = service.spreadsheets().values().get(
            spreadsheetId=spreadsheet_id,
            majorDimension=read_dim,
            range=sheet_name
        ).execute()
        
        return spreadsheet_data['values']
    except Exception as e:
        print(e)
        return None
   

#######################################################
# Get worksheet dimensions
#######################################################

def get_sheet_dims(service, spreadsheet_id, sheet_id):

    spreadsheet = service.spreadsheets().get(spreadsheetId=spreadsheet_id).execute()
    
    for _sheet in spreadsheet['sheets']:
        if _sheet['properties']['sheetId'] == sheet_id:
            sheet_rows = _sheet['properties']['gridProperties']['rowCount']
            sheet_cols = _sheet['properties']['gridProperties']['columnCount']
            break
        
    return sheet_rows, sheet_cols    


#######################################################
# Write data
#######################################################

def write_ws(service, spreadsheet_id, gsheet_range, values, dim = 'ROWS'):
    try:
        values_range_body = {
            'majorDimension': dim,
            'values': values
        }

        service.spreadsheets().values().update(
            spreadsheetId=spreadsheet_id,
            valueInputOption='USER_ENTERED',
            range=gsheet_range,
            body=values_range_body
        ).execute()

        return 
    except Exception as e:
        print(e)
        return None


#######################################################
# Find Functions
#######################################################

def find_END(service, spreadsheet_id, ws_name, row_col, dim = 'ROWS'):
    """
    FInd first END in a row or column

    dim = "ROWS" or "COLUMNS" -> direction of search
    row_col = row number or column letter to search in
    """
    # Extract array of data
    if dim == 'ROWS':
        data = read_ws(service, spreadsheet_id, ws_name + '!' + str(row_col) + ':' + str(row_col))[0]
    elif dim == 'COLUMNS':
        data = read_ws(service, spreadsheet_id, ws_name + '!' + str(row_col) + ':' + str(row_col))
        
        data_tmp = []
        for i in range(len(data)):
            try: 
                data_tmp.append(data[i][0])
            except:
                data_tmp.append('')
        data = data_tmp

    ## get row with END
    END_index = None
    for i in range(len(data)):
        if data[i]=="END":
            END_index = i + 1
            break
    
    return END_index



#######################################################
# Clear data
#######################################################

def clear_data(service, spreadsheet_id, range_to_clear):
    try:
        response = service.spreadsheets().values().clear(
            spreadsheetId=spreadsheet_id,
            range=range_to_clear
        ).execute()

        return 
    except Exception as e:
        print(e)
        return None


def clear_all_cols(service, spreadsheet_id, sheet_id, cols_to_del):

    request_body = {
        'requests': [
            {
                'repeatCell': {
                    'range': {
                        'sheetId': sheet_id,
                        'startColumnIndex': cols_to_del[0] - 1,
                        'endColumnIndex': (cols_to_del[1] - 1) + 1
                    },
                    'fields': '*'
                }
            }
        ]
    }

    run_batchUpdate_request(service, spreadsheet_id, request_body)

    return


#######################################################
# Copy and paste
#######################################################

def copy_paste(service, spreadsheet_id, copy_sheet_id, copy_range, paste_sheet_id, paste_range, request_body = None, paste_type='PASTE_NORMAL'):
    
    rq = {
        'copyPaste': {
            'source': {
                'sheetId': copy_sheet_id,
                'startRowIndex': copy_range[0][0] - 1,
                'endRowIndex': (copy_range[1][0] - 1) + 1,
                'startColumnIndex': copy_range[0][1] - 1,
                'endColumnIndex': (copy_range[1][1] - 1) + 1
            },
            'destination': {
                'sheetId': paste_sheet_id,
                'startRowIndex': paste_range[0][0] - 1,
                'endRowIndex': (paste_range[1][0] - 1) + 1,
                'startColumnIndex': paste_range[0][1] - 1,
                'endColumnIndex': (paste_range[1][1] - 1) + 1
            },
            'pasteType': paste_type
        }
    }

    if request_body == None:
        request_body = {'requests': [rq]}   
        run_batchUpdate_request(service, spreadsheet_id, request_body)
        return None
    else:
        request_body['requests'].append(rq)
        return request_body
