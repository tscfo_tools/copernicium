
# from core.aux_functions import *
# from core.excel_functions import *
from eCopernicium.core.update_functions.update_CC import *
from eCopernicium.core.update_functions.update_company import *
from eCopernicium.core.update_functions.update_format import *
from eCopernicium.core.update_functions.update_region import *
from eCopernicium.core.drive_functions.custom_functions import make_backup_copy


def copernicium(service, user_inputs):

    ##############################################
    ## Save copy
    ##############################################
    make_backup_copy(user_inputs.report_id)
    
    ##############################################
    ## Edit section 
    ##############################################
    ## Edit regions
    if user_inputs.edit_regions:
        update_region_data(service, user_inputs.report_id, user_inputs.regions)

    # Update format
    if user_inputs.edit_format:
        update_format(service, user_inputs.report_id, user_inputs.old_color_1, user_inputs.new_color_1, user_inputs.old_color_2, user_inputs.new_color_2)
            
    ## Edit companies nad their currencies
    if user_inputs.edit_companies:
        update_companies_and_currencies(service, user_inputs.report_id, user_inputs.comp_name, user_inputs.comp_code, user_inputs.currency)
        
    ## Edit CCs
    if user_inputs.edit_cc:
        update_CCs(service, user_inputs.report_id, user_inputs.CC_P, user_inputs.CC_O)


    return

