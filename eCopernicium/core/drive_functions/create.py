
from eCopernicium.core.drive_functions.get_data import list_files, get_if_trashed_file
from eCopernicium.core.drive_functions.delete import delete_forever


#########################################
# Create request
#########################################
def create_request(service, request_body):
    try:    
        response = service.files().create(
            body=request_body
        ).execute()
        return response
    except Exception as e:
        print(e)
        return None


#########################################
# Create a folder in a folder
#########################################

def create_folder(service, folder_list, root_folder_id = []):
    
    for i in range(len(folder_list)):
        request_body = {
            'name': folder_list[i],
            'mimeType': 'application/vnd.google-apps.folder'
        }
        if len(root_folder_id) == 1: request_body['parents'] = root_folder_id
        if len(root_folder_id) > 1: request_body['parents'] = [root_folder_id[i]]
    
        create_request(service, request_body)

    return

#########################################
# Create a folder in a folder -> Optimized
# -> Look if a folder with the same name is in the parent folder
# -> Look if a delete file with the same name has been cleaned forever
# -> It returns the new folder id
#########################################

def create_folder_opt(drive_service, parent_folder_id, folder_name):
    
    # Create folder if not exists
    ## List files/folders in parent folder
    files_name, files_id, files_type = list_files(drive_service, parent_folder_id)
    ## Look if exists, else create it 
    if folder_name not in files_name:
        create_folder(drive_service, [folder_name], [parent_folder_id])
    else:
        # If the file name exists, check is a folder -> if not, create the folder
        if files_type[files_name.index(folder_name)] != 'folder':
            create_folder(drive_service, [folder_name], [parent_folder_id])  
        # Check if it is trashed or not -> if trashed, delete from trash and create a new folder
        else:
            folder_id = files_id[files_name.index(folder_name)]
            if get_if_trashed_file(drive_service, folder_id):
                # delete forever
                delete_forever(drive_service, folder_id)
                # create new vc folder
                create_folder(drive_service, [folder_name], [parent_folder_id])
        
    
    # get folder id
    files_name, files_id, files_type = list_files(drive_service, parent_folder_id)
    folder_id = files_id[files_name.index(folder_name)]
    
    return folder_id



#########################################
# Create a file in a folder
#########################################

def create_file(service, file_list, file_type, root_folder_id = []):
    
    for i in range(len(file_list)):
        request_body = {
            'name': file_list[i],
            'mimeType': file_type[i]
        }
        if len(root_folder_id) == 1: request_body['parents'] = root_folder_id
        if len(root_folder_id) > 1: request_body['parents'] = [root_folder_id[i]]
    
        create_request(service, request_body)

        return