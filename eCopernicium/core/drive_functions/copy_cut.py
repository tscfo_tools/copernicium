

#########################################
# Copy files
#########################################

def copy_file(service, src_file_id, dst_folder_ids, dst_file_names, description = None):
    try:
        for i in range(len(dst_file_names)):
            request_body = {
                'name': dst_file_names[i],
                'parents': [dst_folder_ids[i]],
                'starred': True
            }
            
            # Add description
            if description != None: request_body['description'] = description[i]
            
        # Apply request 
            service.files().copy(
                fileId=src_file_id,
                body=request_body
            ).execute()
            
        return
    except Exception as e:
        print(e)
        return None