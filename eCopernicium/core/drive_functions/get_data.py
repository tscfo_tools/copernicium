
#########################################
# Get file id
#########################################
def get_file_id(file_url):
    try:
        return file_url[file_url.find('/d/') + 3:file_url.find('/d/') + 3 + file_url[file_url.find('/d/') + 3:].find('/')]
    except:
        return 'Error getting file id'


#########################################
# Get folder id
#########################################
def get_folder_id(file_url):
    try:
        return file_url[file_url.find('/folders/') + 9:]
    except:
        return 'Error getting folder id'


#########################################
# Get parent folder id
#########################################
def get_parent_folder_id(drive_service, file_id):
    try:
        return drive_service.files().get(fileId=file_id,fields="parents").execute()['parents'][0]
    except Exception as e:
        print(e)
        return None


#########################################
# Get file name from id
#########################################

def get_file_name_from_id(drive_service, file_id): 
    try:
        # get file name from id
        return drive_service.files().get(fileId=file_id, fields='name').execute()['name']
    except Exception as e:
        print(e)
        return None


#########################################
# Get file name from url
#########################################

def get_file_name_from_url(drive_service, file_url): 
    try:
        # get file id from url
        file_id = get_file_id(file_url)
        # get file name from id
        return drive_service.files().get(fileId=file_id, fields='name').execute()['name']
    except Exception as e:
        print(e)
        return None


#########################################
# Get if a file or folder has been move to trash
#########################################
def get_if_trashed_file(drive_service, file_id):
    try:
        return drive_service.files().get(fileId=file_id, fields="trashed").execute()['trashed']
    except Exception as e:
        print(e)
        return None


#########################################
# Get all files and folders in a folder
#########################################

def list_files(service, folder_id):
    try:
        query = f"parents = '{folder_id}'"
        
        response = service.files().list(q=query).execute()
        files = response.get('files')
        nextPageToken = response.get('nextPageToken')
        
        while nextPageToken:
            response = service.files().list(q=query).execute()
            files.extend(response.get('files'))
            nextPageToken = response.get('nextPageToken')
        
        files_name = [files[i]['name'] for i in range(len(files))]
        files_id = [files[i]['id'] for i in range(len(files))]
        files_type = [files[i]['mimeType'].split('.')[-1] for i in range(len(files))]
        
        return files_name, files_id, files_type
    
    except Exception as e:
        print(e)
        return None