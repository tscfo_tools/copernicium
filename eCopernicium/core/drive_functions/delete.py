

#########################################
# Delete a file/folder forever
#########################################

def delete_forever(drive_service, file_id):
    try:
        # delete forever
        drive_service.files().delete(fileId=file_id).execute()
        return
        
    except Exception as e:
        print(e)
        return None
