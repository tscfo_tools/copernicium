
from eCopernicium.core.drive_functions import get_data as drive_data
from eCopernicium.core.drive_functions import copy_cut as drive_copy

from eCopernicium.client_secret_account_json import *
from eCopernicium.core.sheets_functions.google_apis import Create_Service

def make_backup_copy(reporting_id):

    API_NAME = 'drive'
    API_VERSION = 'v3'
    SCOPES = ['https://www.googleapis.com/auth/drive']

    # connection with google drive api
    drive_service = Create_Service(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES)
    
    # get id of parent folder
    parent_folder_id = drive_data.get_parent_folder_id(drive_service, reporting_id)
    
    # get reporting spreadsheet name
    reporting_name = drive_data.get_file_name_from_id(drive_service, reporting_id)
    
    # defining name of copied file -> name + '_backup_copy'
    new_name = reporting_name + '_backup_copy'
    
    # copy the file to save a a backup copy
    drive_copy.copy_file(drive_service, reporting_id, [parent_folder_id], [new_name])
    
    return
