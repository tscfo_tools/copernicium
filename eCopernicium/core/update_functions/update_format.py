
from eCopernicium.core.sheets_functions import spreadsheets_and_worksheets as gsheets
from eCopernicium.core.sheets_functions import formats
from eCopernicium.core.sheets_functions import Visualization as visualization
from eCopernicium.core.sheets_functions.requests import *
from eCopernicium.core.aux_functions import hex_to_rgb



def update_format(service, spreadsheet_id, prev_main_color_input, new_main_color_input, prev_secondary_color_input, new_secondary_color_input):

    # 0.1. Convert colors code from hex to rgb
    prev_main_color_gs = list(map(lambda x: round(x/255, 4), hex_to_rgb(prev_main_color_input)))
    prev_secondary_color_gs = list(map(lambda x: round(x/255, 4), hex_to_rgb(prev_secondary_color_input)))
    
    new_main_color_gs = list(map(lambda x: round(x/255, 4), hex_to_rgb(new_main_color_input)))
    new_secondary_color_gs = list(map(lambda x: round(x/255, 4), hex_to_rgb(new_secondary_color_input)))

    # 0.2. Get sheets data
    sheets_info = gsheets.get_sheets_info(service, spreadsheet_id)
    
    
    # 2. For each sheet, build requests for updating format
    request_body = {'requests': []}
    for sheet in sheets_info:
        
        # Get all sheet info
        sheet_info = service.spreadsheets().get(
            spreadsheetId=spreadsheet_id,
            ranges=sheet,
            includeGridData=True
        ).execute()
        
        
        # Change tab colors
        if 'tab_color' in sheets_info[sheet].keys():
            
            tab_rgb_array = [round(sheets_info[sheet]['tab_color']['red'], 4) if 'red' in sheets_info[sheet]['tab_color'].keys() else 0.0, \
                                        round(sheets_info[sheet]['tab_color']['green'], 4) if 'green' in sheets_info[sheet]['tab_color'].keys() else 0.0, \
                                        round(sheets_info[sheet]['tab_color']['blue'], 4) if 'blue' in sheets_info[sheet]['tab_color'].keys() else 0.0]
              
            # Background cell color
            ## Main color  
            if tab_rgb_array == prev_main_color_gs:
                if "-Region" in sheet:
                    color_to_apply = {'red': new_main_color_gs[0], 'green': new_main_color_gs[1],'blue': new_main_color_gs[2]}
                    request_body = visualization.hide_sheet(service, spreadsheet_id, sheet, sheets_info[sheet]['sheetId'], sheets_info[sheet]['index'], sheets_info[sheet]['rows'], sheets_info[sheet]['cols'], color_to_apply, request_body = request_body)
                else:
                    request_body = gsheets.define_sheet_props(service, spreadsheet_id, sheet, sheets_info[sheet]['sheetId'], sheets_info[sheet]['index'], rowCount=sheets_info[sheet]['rows'], columnCount=sheets_info[sheet]['cols'], tab_color = new_main_color_gs, request_body = request_body)
            
            ## Secondary color  
            if tab_rgb_array == prev_secondary_color_gs:
                if "-Region" in sheet:
                    color_to_apply = {'red': new_secondary_color_gs[0], 'green': new_secondary_color_gs[1],'blue': new_secondary_color_gs[2]}
                    request_body = visualization.hide_sheet(service, spreadsheet_id, sheet, sheets_info[sheet]['sheetId'], sheets_info[sheet]['index'], sheets_info[sheet]['rows'], sheets_info[sheet]['cols'], color_to_apply, request_body = request_body)
                else:
                    request_body = gsheets.define_sheet_props(service, spreadsheet_id, sheet, sheets_info[sheet]['sheetId'], sheets_info[sheet]['index'], rowCount=sheets_info[sheet]['rows'], columnCount=sheets_info[sheet]['cols'], tab_color = new_secondary_color_gs, request_body = request_body)
                  
        
        # Change font and background color
        for i in range(sheets_info[sheet]['rows']):
            for j in range(sheets_info[sheet]['cols']):
                
                # Background cell color
                try:
                    bg_cell_rgb_key = sheet_info['sheets'][0]['data'][0]['rowData'][i]['values'][j]['effectiveFormat']['backgroundColorStyle']['rgbColor']
                    
                    bg_cell_rgb_array = [round(bg_cell_rgb_key['red'], 4) if 'red' in bg_cell_rgb_key.keys() else 0.0, \
                                          round(bg_cell_rgb_key['green'], 4) if 'green' in bg_cell_rgb_key.keys() else 0.0, \
                                          round(bg_cell_rgb_key['blue'], 4) if 'blue' in bg_cell_rgb_key.keys() else 0.0]
                    
                    # Background cell color
                    ## Main color  
                    if bg_cell_rgb_array == prev_main_color_gs:
                        range_to_format = [[i + 1, j + 1], [i + 1, j + 1]]
                        request_body = formats.formatting_background_color(service, spreadsheet_id, sheets_info[sheet]['sheetId'], range_to_format, request_body = request_body, rgb_pattern = new_main_color_gs)
                    
                    ## Secondary color  
                    if bg_cell_rgb_array == prev_secondary_color_gs:
                        range_to_format = [[i + 1, j + 1], [i + 1, j + 1]]
                        request_body = formats.formatting_background_color(service, spreadsheet_id, sheets_info[sheet]['sheetId'], range_to_format, request_body = request_body, rgb_pattern = new_secondary_color_gs)
        
                except: pass
            
                # Titles font color (search in first 5 rows)
                if i < 5:
                    try:
                        text_cell_rgb_key = sheet_info['sheets'][0]['data'][0]['rowData'][i]['values'][j]['userEnteredFormat']['textFormat']['foregroundColorStyle']['rgbColor']
                        
                        text_cell_rgb_array = [round(text_cell_rgb_key['red'], 4) if 'red' in text_cell_rgb_key.keys() else 0.0, \
                                              round(text_cell_rgb_key['green'], 4) if 'green' in text_cell_rgb_key.keys() else 0.0, \
                                              round(text_cell_rgb_key['blue'], 4) if 'blue' in text_cell_rgb_key.keys() else 0.0]
                        
                        text_fontsize = sheet_info['sheets'][0]['data'][0]['rowData'][i]['values'][j]['userEnteredFormat']['textFormat']['fontSize']
                            
                        # Background cell color
                        ## Main color  
                        if text_cell_rgb_array == prev_main_color_gs:
                            range_to_format = [[i + 1, j + 1], [i + 1, j + 1]]
                            request_body = formats.formatting_text_color(service, spreadsheet_id, sheets_info[sheet]['sheetId'], range_to_format, request_body = request_body, rgb_pattern = new_main_color_gs, bold_op=True, fontsize = text_fontsize)
                                           
                    except: pass
        
        
        # Change charts background color in sheet 'REPORTING->'
        if sheet == 'REPORTING->':
            try: 
                if 'charts' in sheet_info['sheets'][0].keys():
                    charts_info = sheet_info['sheets'][0]['charts']
                    
                    for k in range(len(charts_info)):
                        bg_chart_rgb_key = charts_info[k]['spec']['backgroundColorStyle']['rgbColor']
                        bg_chart_rgb_key_2 = charts_info[k]['spec']['backgroundColor']
                        
                        bg_chart_rgb_array = [round(bg_chart_rgb_key ['red'], 4) if 'red' in bg_chart_rgb_key .keys() else 0.0, \
                                              round(bg_chart_rgb_key ['green'], 4) if 'green' in bg_chart_rgb_key .keys() else 0.0, \
                                              round(bg_chart_rgb_key ['blue'], 4) if 'blue' in bg_chart_rgb_key .keys() else 0.0]
                        
                        bg_chart_rgb_array_2 = [round(bg_chart_rgb_key_2 ['red'], 4) if 'red' in bg_chart_rgb_key_2 .keys() else 0.0, \
                                              round(bg_chart_rgb_key_2 ['green'], 4) if 'green' in bg_chart_rgb_key_2 .keys() else 0.0, \
                                              round(bg_chart_rgb_key_2 ['blue'], 4) if 'blue' in bg_chart_rgb_key_2 .keys() else 0.0]
                        
                        if (bg_chart_rgb_array == prev_main_color_gs) or bg_chart_rgb_array_2 == prev_main_color_gs:
                            request_body = formats.update_chart_bg_color(service, spreadsheet_id, charts_info[k]['chartId'], charts_info[k]['spec'], bg_color = new_main_color_gs, request_body = request_body)
                        
            except: pass
        
        
    run_batchUpdate_request(service, spreadsheet_id, request_body)
    
    return