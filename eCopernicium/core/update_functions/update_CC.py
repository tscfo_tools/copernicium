
from eCopernicium.core.sheets_functions import spreadsheets_and_worksheets as gsheets
from eCopernicium.core.sheets_functions import read_and_write as gsheet_data
from eCopernicium.core.sheets_functions import Visualization as visualization
from eCopernicium.core.sheets_functions import interactions
from eCopernicium.core.sheets_functions.requests import *
from eCopernicium.core.aux_functions import colnum_string, extract_int



def hide_PL_xxx_rows(service, spreadsheet_id, request_body, sheets_main_info, sheet_name):
    
    sheet_data = gsheet_data.read_ws(service, spreadsheet_id, sheet_name, read_dim = 'ROWS')
    var_col = None
    for i in range(len(sheet_data)):
        for j in range(len(sheet_data[i])):
            if "Reporting" in sheet_data[i][j] or "Forecast" in sheet_data[i][j]:
                var_col = j + 1
                break
        if var_col != None: break    
    
    
    var_data = gsheet_data.read_ws(service, spreadsheet_id, sheet_name + '!' + colnum_string(var_col) + ':' + colnum_string(var_col), read_dim = 'COLUMNS')[0]
    
    ### Find blocks of "Personnel costs" and "Operational costs"
    PL_ranges_ini = [(i + 1) for i in range(len(var_data)) if var_data[i] == "Personnel costs"]
    PL_ranges_ini = PL_ranges_ini + [(i + 1) for i in range(len(var_data)) if var_data[i] == "Operating costs"]
    PL_ranges_end = [(PL_ranges_ini[i] + var_data[PL_ranges_ini[i]:].index('')) for i in range(len(PL_ranges_ini))]
    
    #### Look for "xxx"s in each block, and if exists -> hide the row
    for i in range(len(PL_ranges_ini)):
        ##### Unhide all rows in CC range
        request_body = visualization.apply_unhide(service, spreadsheet_id, sheets_main_info[sheet_name]['sheetId'], PL_ranges_ini[i], PL_ranges_end[i], request_body = request_body, dim = "ROWS")
        
        if "xxx" in var_data[PL_ranges_ini[i] - 1: PL_ranges_end[i]]:
            ini_row_to_hide = PL_ranges_ini[i] - 1 + var_data[PL_ranges_ini[i] - 1: PL_ranges_end[i]].index('xxx')
            end_row_to_hide = PL_ranges_end[i]
            # Hide request for this CC block
            request_body = visualization.apply_hide(service, spreadsheet_id, sheets_main_info[sheet_name]['sheetId'], ini_row_to_hide, end_row_to_hide, request_body = request_body, dim = "ROWS")    
    
    return request_body



    


def hide_PC_and_OC_xxx_rows(service, spreadsheet_id, request_body, sheets_main_info, sheet_name = 'PC', ref_word = "Personnel cost"):

    sheet_data = gsheet_data.read_ws(service, spreadsheet_id, sheet_name, read_dim = 'ROWS')
    var_col = None
    for i in range(len(sheet_data)):
        if ref_word in sheet_data[i]:
            var_col = sheet_data[i].index(ref_word) + 1
            break
    

    sheet_info = service.spreadsheets().get(
                spreadsheetId=spreadsheet_id,
                ranges= sheet_name + '!' + colnum_string(var_col) + ':' + colnum_string(var_col),
                includeGridData=True
            ).execute()
    
    
    
    custom_data_info = []
    for i in range(len(sheet_info['sheets'][0]['data'][0]['rowData'])):
        try:
            try:
                cell_text = sheet_info['sheets'][0]['data'][0]['rowData'][i]['values'][0]['formattedValue']
            except:
                cell_text = ''
            try:
                bg_color = sheet_info['sheets'][0]['data'][0]['rowData'][i]['values'][0]['userEnteredFormat']['backgroundColor']
            except: 
                bg_color = ''
            custom_data_info.append([cell_text, 
            sheet_info['sheets'][0]['data'][0]['rowData'][i]['values'][0]['userEnteredFormat']['textFormat']['foregroundColorStyle'],
            bg_color])
        except:
            custom_data_info.append([None, None, None])
    
    
    request_body = visualization.apply_unhide(service, spreadsheet_id, sheets_main_info[sheet_name]['sheetId'], 1, sheets_main_info[sheet_name]['cols'], request_body = request_body, dim = "ROWS")
    for i in range(len(custom_data_info)):
        if custom_data_info[i][0] == 'xxx' and custom_data_info[i][1] == {'themeColor': 'TEXT'} and custom_data_info[i][2] == '':
            request_body  = visualization.apply_hide(service, spreadsheet_id, sheets_main_info[sheet_name]['sheetId'], i, i + 1, request_body = request_body, dim = "ROWS")


    return request_body

def manage_OC_sections_in_OC(service, spreadsheet_id, request_body, sheets_main_info, CC_O_input, sheet_name = 'OC', ref_word = "Operating cost"):
 
    # 1. Get necessary information from var column in OC sheet
    ## 1.1. Get var column
    sheet_data = gsheet_data.read_ws(service, spreadsheet_id, sheet_name, read_dim = 'ROWS')
    var_col = None
    for i in range(len(sheet_data)):
        if ref_word in sheet_data[i]:
            var_col = sheet_data[i].index(ref_word) + 1
            break
    
    # 1.2. Get necessary information from this column: text, font color, background color and formula
    sheet_info = service.spreadsheets().get(
                spreadsheetId=spreadsheet_id,
                ranges= sheet_name + '!' + colnum_string(var_col) + ':' + colnum_string(var_col),
                includeGridData=True
            ).execute()
    
    custom_data_info = []
    for i in range(len(sheet_info['sheets'][0]['data'][0]['rowData'])):
        try:
            cell_text = sheet_info['sheets'][0]['data'][0]['rowData'][i]['values'][0]['formattedValue']
        except:
            cell_text = ''
        try:
            font_color = sheet_info['sheets'][0]['data'][0]['rowData'][i]['values'][0]['userEnteredFormat']['textFormat']['foregroundColorStyle']
        except:
            font_color = ''
        try:
            bg_color = sheet_info['sheets'][0]['data'][0]['rowData'][i]['values'][0]['userEnteredFormat']['backgroundColor']
        except: 
            bg_color = ''
        try:
            formula = sheet_info['sheets'][0]['data'][0]['rowData'][i]['values'][0]['userEnteredValue']['formulaValue']
        except:
            formula = ''
        custom_data_info.append([cell_text, font_color, bg_color, formula])
    
        
    
    # 1.3. Get location of OPERATING COSTS section (between 'OPERATING COSTS' and 'OTHER RESULTS')
    for i in range(len(custom_data_info)):
        if 'OPERATING COSTS' in custom_data_info[i]: 
            OC_section_ini = i
            break
    for i in range(len(custom_data_info)):
        if 'OTHER RESULTS' in custom_data_info[i]: 
            OC_section_end = i - 1
            break
    
    # 1.4. Get data from OC sections: text, initial row and formula 
    OC_sections = []
    for i in range(OC_section_ini + 1, OC_section_end):
        if custom_data_info[i][2] != '':
            OC_sections.append([custom_data_info[i][0], i, custom_data_info[i][-1]])
    
    
    # 2. Request for unhide all rows
    request_body = {'requests': []}
    request_body = visualization.apply_unhide(service, spreadsheet_id, sheets_main_info['OC']['sheetId'], OC_section_ini + 1, OC_section_end + 1, request_body = request_body, dim = "ROWS")
    
    

    # 3. IF CCs are less than available sections, hide the unnecesary sections.
    if len(CC_O_input) < len(OC_sections):
        ## 3.1. Find sections with title "xxx"
        for i in range(len(custom_data_info)):
            if 'xxx' in OC_sections[i]: 
                first_row_to_hide = OC_sections[i][1]
                break
        
        last_row_to_hide = OC_section_end + 1
        # 3.2. Request for hiding those sections
        request_body = visualization.apply_hide(service, spreadsheet_id, sheets_main_info['OC']['sheetId'], first_row_to_hide, last_row_to_hide, request_body = request_body, dim = "ROWS")
        
    # 3.3. Launch request for unhide everything and if necessary hide unnecesary sections
    run_batchUpdate_request(service, spreadsheet_id, request_body)
    
    
    
    # 4. If it is necessary to add new sections
    if len(CC_O_input) > len(OC_sections):
        ## 4.1. Define new sections to add and rows per section
        new_sections = len(CC_O_input) - len(OC_sections)
        rows_per_section = (OC_section_end + 1) - OC_sections[-1][1]
        ## 4.2. Insert necessary rows before "OTHER RESULTS
        interactions.insert_rows(service, spreadsheet_id, sheets_main_info['OC']['sheetId'], OC_section_end + 2, (OC_section_end + 2) + new_sections * rows_per_section)
        
        ## 4.3. Copy new sections
        ###  4.3.1 Copy first added section
        copy_range = [[OC_sections[-1][1] + 1, 1], [OC_section_end + 1, sheets_main_info['OC']['cols']]]
        paste_range = [[(OC_section_end + 1) + 1, 1], [(OC_section_end + 1) + rows_per_section, sheets_main_info['OC']['cols']]]
        gsheet_data.copy_paste(service, spreadsheet_id, sheets_main_info['OC']['sheetId'], copy_range, sheets_main_info['OC']['sheetId'], paste_range, request_body = None, paste_type='PASTE_NORMAL')
        #### 4.3.1.1 Change title formula
        new_formula_row = extract_int(OC_sections[-1][2]) + 1
        new_formula = OC_sections[-1][2].replace(str(new_formula_row - 1), str(new_formula_row))
        gsheet_data.write_ws(service, spreadsheet_id, 'OC!' + colnum_string(var_col) + str(paste_range[0][0]), [[new_formula]])
        
        ### 4.3.2. Copy the rest of sections
        for i in range(1, new_sections):
            copy_range = paste_range
            paste_range = [[paste_range[1][0] + 1, 1], [paste_range[1][0] + rows_per_section, sheets_main_info['OC']['cols']]]
            gsheet_data.copy_paste(service, spreadsheet_id, sheets_main_info['OC']['sheetId'], copy_range, sheets_main_info['OC']['sheetId'], paste_range, request_body = None, paste_type='PASTE_NORMAL')
            #### 4.3.2.1 Change title formula
            new_formula_row = new_formula_row + 1
            new_formula = new_formula.replace(str(new_formula_row - 1), str(new_formula_row))
            gsheet_data.write_ws(service, spreadsheet_id, 'OC!' + colnum_string(var_col) + str(paste_range[0][0]), [[new_formula]])


    return



###########################################
#### Main function: update CCs
###########################################

def update_CCs(service, spreadsheet_id, CC_P_input, CC_O_input, aux_sheet= 'Aux', CC_P_name = 'CC_Personnel', CC_O_name = 'CC_Operating'):
    # 0. Get basic info from all sheets: sheet name, index, id, total number of rows and cols
    sheets_main_info = gsheets.get_sheets_info(service, spreadsheet_id)
    
    # 1. Update Regions info in "Aux" sheet
    ## 1.1. Get Named ranges in "Aux" sheet
    ### 1.1.1. Get Aux sheet id
    aux_sheet_id = gsheets.get_sheetId(spreadsheet_id, [aux_sheet])[0]
    ### 1.1.2. Get info about named ranges 
    named_range_info = gsheets.get_named_range_info(service, spreadsheet_id, aux_sheet)
    
    
    ## 1.2. Update info for Personnel costs
    ### 1.2.1. Write new Personnel cost centers in "Aux" sheet
    prev_CC_P = gsheet_data.read_ws(service, spreadsheet_id, CC_P_name, read_dim = 'COLUMNS')
    CC_P_output = [ [prev_CC_P[0][0]] + [CC_P_input[i] if i < len(CC_P_input) else 'xxx' for i in range(len(prev_CC_P[0]) - 1)]]
    gsheet_data.write_ws(service, spreadsheet_id, CC_P_name, CC_P_output, dim = 'COLUMNS')
    
    ### 1.2.2. Get Personnel costs region location: intial and last cell
    region_ini_cell = [named_range_info[CC_P_name]['startRowIndex'] + 1, named_range_info[CC_P_name]['startColumnIndex'] + 1]
    region_end_cell = [named_range_info[CC_P_name]['endRowIndex'], named_range_info[CC_P_name]['endColumnIndex']]
    
    ### 1.2.3. Save info for hide rows with no Personnel CC ('xxx' in "CC_Personnel" range)
    request_body = {'requests': []}
    #### Request: unhide Personnel CC region
    request_body = visualization.apply_unhide(service, spreadsheet_id, aux_sheet_id, region_ini_cell[0] - 1, region_end_cell[0], request_body = request_body, dim = "ROWS")
    #### Request: hide unnecesary Personnel CC region rows ('xxx')
    request_body = visualization.apply_hide(service, spreadsheet_id, aux_sheet_id, region_ini_cell[0] + len(CC_P_input), region_end_cell[0], request_body = request_body, dim = "ROWS")
    
    
    ## 1.3. Update info for Operating costs
    ### 1.3.1. Write new Operating cost centers in "Aux" sheet
    prev_CC_O = gsheet_data.read_ws(service, spreadsheet_id, CC_O_name, read_dim = 'COLUMNS')
    CC_O_output = [ [prev_CC_O[0][0]] + [CC_O_input[i] if i < len(CC_O_input) else 'xxx' for i in range(len(prev_CC_O[0]) - 1)]]
    gsheet_data.write_ws(service, spreadsheet_id, CC_O_name, CC_O_output, dim = 'COLUMNS')
    
    ### 1.3.2. Get Operating costs region location: intial and last cell
    region_ini_cell = [named_range_info[CC_O_name]['startRowIndex'] + 1, named_range_info[CC_O_name]['startColumnIndex'] + 1]
    region_end_cell = [named_range_info[CC_O_name]['endRowIndex'], named_range_info[CC_O_name]['endColumnIndex']]
    
    ### 1.3.3. Save info for hide rows with no Operating CC ('xxx' in "CC_Operating" range)
    #### Request: unhide Operating CC region
    request_body = visualization.apply_unhide(service, spreadsheet_id, aux_sheet_id, region_ini_cell[0] - 1, region_end_cell[0], request_body = request_body, dim = "ROWS")
    #### Request: hide unnecesary Operating CC region rows ('xxx')
    request_body = visualization.apply_hide(service, spreadsheet_id, aux_sheet_id, region_ini_cell[0] + len(CC_O_input), region_end_cell[0], request_body = request_body, dim = "ROWS")
    
    ### 1.4. Apply unhide and hide request in "Aux" sheet
    run_batchUpdate_request(service, spreadsheet_id, request_body)
    
    
    # 2. Hide rows without CC ("xxx") in "PL", "P&L", "PC" and "OC" sheets
    ## 2.0. Initialize request
    request_body = {'requests': []}
    ## 2.1. Add request for hiding rows in "PL" and "P&L" sheets
    for sheet_name in list(sheets_main_info.keys()):
        if sheet_name == 'PL' or sheet_name.startswith('PL-') or sheet_name == 'P&L' or sheet_name.startswith('P&L-'):
            request_body = hide_PL_xxx_rows(service, spreadsheet_id, request_body, sheets_main_info, sheet_name)
    ## 2.2. Add request for hiding rows in "PC" and "OC" sheets
    request_body = hide_PC_and_OC_xxx_rows(service, spreadsheet_id, request_body, sheets_main_info, sheet_name = 'PC', ref_word = "Personnel cost")
    request_body = hide_PC_and_OC_xxx_rows(service, spreadsheet_id, request_body, sheets_main_info, sheet_name = 'OC', ref_word = "Operating cost")
    ## 2.3. Run request
    run_batchUpdate_request(service, spreadsheet_id, request_body)    
    ## 2.4. Add or hide OC sections in "OC" sheet
    manage_OC_sections_in_OC(service, spreadsheet_id, request_body, sheets_main_info, CC_O_input, sheet_name = 'OC', ref_word = "Operating cost")

    return
