
from eCopernicium.core.sheets_functions import spreadsheets_and_worksheets as gsheets
from eCopernicium.core.sheets_functions import read_and_write as gsheet_data
from eCopernicium.core.sheets_functions import Visualization as visualization
from eCopernicium.core.sheets_functions.requests import *
from eCopernicium.core.aux_functions import colnum_string




def update_region_data(service, spreadsheet_id, region_input, aux_sheet= 'Aux', max_regions = 3, KPIs_region_name = 'KPIs_Regions', PL_region_name = 'PL_Regions'):


    # 1. Update Regions info in "Aux" sheet
    ## 1.1. Get Named ranges in "Aux" sheet
    ### 1.1.1. Get Aux sheet id
    aux_sheet_id = gsheets.get_sheetId(spreadsheet_id, [aux_sheet])[0]
    ### 1.1.2. Get info about named ranges 
    named_range_info = gsheets.get_named_range_info(service, spreadsheet_id, aux_sheet)
    
    
    ## 1.2. Update infor for KPIs regions
    ### 1.2.1. Get KPIs region location: intial and last cell
    region_ini_cell = [named_range_info[KPIs_region_name]['startRowIndex'] + 1, named_range_info[KPIs_region_name]['startColumnIndex'] + 1]
    region_end_cell = [named_range_info[KPIs_region_name]['endRowIndex'], named_range_info[KPIs_region_name]['endColumnIndex']]
    ### 1.2.2. Write new KPIs regions in "Aux" sheet
    region_output = [region_input[i] if i < len(region_input) else '' for i in range(max_regions)]
    gsheet_data.write_ws(service, spreadsheet_id, aux_sheet + '!' + colnum_string(region_ini_cell[1] + 1) + str(region_ini_cell[0] + 1), [region_output], dim = 'COLUMNS')
    
    ### 1.2.3. Save info for hide rows with no regions in KPIs named range
    request_body = {'requests': []}
    #### Request: unhide KPIs region
    request_body = visualization.apply_unhide(service, spreadsheet_id, aux_sheet_id, region_ini_cell[0] - 1, region_end_cell[0], request_body = request_body, dim = "ROWS")
    #### Request: hide unnecesary KPIs region rows
    request_body = visualization.apply_hide(service, spreadsheet_id, aux_sheet_id, region_ini_cell[0] + len(region_input), region_end_cell[0] - 1, request_body = request_body, dim = "ROWS")
    
    ## 1.3. Update infor for PL regions (HQ not modified)
    ### 1.3.1. Get PL region location: intial and last cell
    region_ini_cell = [named_range_info[PL_region_name]['startRowIndex'] + 1, named_range_info[PL_region_name]['startColumnIndex'] + 1]
    region_end_cell = [named_range_info[PL_region_name]['endRowIndex'], named_range_info[PL_region_name]['endColumnIndex']]
    ### 1.3.2. Write new KPIs regions in "Aux" sheet
    region_output = [[region_input[i], region_input[i]] if i < len(region_input) else ['', ''] for i in range(max_regions)]
    gsheet_data.write_ws(service, spreadsheet_id, aux_sheet + '!' + colnum_string(region_ini_cell[1] + 1) + str(region_ini_cell[0] + 2), region_output, dim = 'ROWS')
    
    ### 1.3.3. Save info for hide rows with no regions in PL named range
    #### Request: unhide PLs region
    request_body = visualization.apply_unhide(service, spreadsheet_id, aux_sheet_id, region_ini_cell[0] - 1, region_end_cell[0], request_body = request_body, dim = "ROWS")
    #### Request: hide unnecesary PLs region rows
    request_body = visualization.apply_hide(service, spreadsheet_id, aux_sheet_id, region_ini_cell[0] + len(region_input) + 1, region_end_cell[0] - 1, request_body = request_body, dim = "ROWS")
    
    ### 1.4. Apply unhide and hide request in "Aux" sheet
    run_batchUpdate_request(service, spreadsheet_id, request_body)
    
    ##########################################
    # 2. Update Regions sheets in Spreadsheet
    ## 2.0. Get basic info from all sheets: sheet name, index, id, total number of rows and cols
    sheets_main_info = gsheets.get_sheets_info(service, spreadsheet_id)
    
    ## 2.1. Rename regions sheets as "...-Region1", "...-Region2" and "...-Region3"
    request_body = {'requests': []}  
    for ini_str in ["KPIs-", "Metrics-", "PL-", "P&L-"]:
        region_ind = 1
        for sheet_name in list(sheets_main_info.keys()):
            ## Rename sheets regions as "Region1, Region2, etc
            if ini_str in sheet_name and "HQ" not in sheet_name:
                # rename_sheet(service, spreadsheet_id, new_sheet_name, old_sheet_name, sheet_id, sheet_index, rowCount = 1000, columnCount = 100, tab_color = None, request_body = None):   
                try:
                    request_body = gsheets.rename_sheet(service, spreadsheet_id, ini_str + "Region" + str(region_ind), sheet_name, sheets_main_info[sheet_name]['sheetId'], sheets_main_info[sheet_name]['index'], sheets_main_info[sheet_name]['rows'], sheets_main_info[sheet_name]['cols'], sheets_main_info[sheet_name]['tab_color'], request_body = request_body)
                except:
                    request_body = gsheets.rename_sheet(service, spreadsheet_id, ini_str + "Region" + str(region_ind), sheet_name, sheets_main_info[sheet_name]['sheetId'], sheets_main_info[sheet_name]['index'], sheets_main_info[sheet_name]['rows'], sheets_main_info[sheet_name]['cols'], request_body = request_body)
                region_ind += 1
    run_batchUpdate_request(service, spreadsheet_id, request_body)
                
    ## 2.2. Rename regions sheets with the new ones
    ### 2.2.0 Reload info with new region sheet names
    sheets_main_info = gsheets.get_sheets_info(service, spreadsheet_id)
    ## 2.2.1. Rename regions sheets with the new region names
    request_body = {'requests': []}  
    for ini_str in ["KPIs-", "Metrics-", "PL-", "P&L-"]:
        for i in range(len(region_input)):
            sheet_name = ini_str + "Region" + str(i + 1)
            try:
                request_body = gsheets.rename_sheet(service, spreadsheet_id, ini_str + region_input[i], sheet_name, sheets_main_info[sheet_name]['sheetId'], sheets_main_info[sheet_name]['index'], sheets_main_info[sheet_name]['rows'], sheets_main_info[sheet_name]['cols'], sheets_main_info[sheet_name]['tab_color'], request_body = request_body)
            except:
                request_body = gsheets.rename_sheet(service, spreadsheet_id, ini_str + region_input[i], sheet_name, sheets_main_info[sheet_name]['sheetId'], sheets_main_info[sheet_name]['index'], sheets_main_info[sheet_name]['rows'], sheets_main_info[sheet_name]['cols'], request_body = request_body)
        if len(region_input) < max_regions:
            for i in range(len(region_input), max_regions):
                sheet_name = ini_str + "Region" + str(i + 1)
                try:
                    request_body = visualization.hide_sheet(service, spreadsheet_id, sheet_name, sheets_main_info[sheet_name]['sheetId'], sheets_main_info[sheet_name]['index'], sheets_main_info[sheet_name]['rows'], sheets_main_info[sheet_name]['cols'], sheets_main_info[sheet_name]['tab_color'], request_body = request_body)           
                except:
                    request_body = visualization.hide_sheet(service, spreadsheet_id, sheet_name, sheets_main_info[sheet_name]['sheetId'], sheets_main_info[sheet_name]['index'], sheets_main_info[sheet_name]['rows'], sheets_main_info[sheet_name]['cols'], request_body = request_body)           
    
    run_batchUpdate_request(service, spreadsheet_id, request_body)        
    
    return