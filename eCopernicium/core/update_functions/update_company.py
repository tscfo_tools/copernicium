
from eCopernicium.core.sheets_functions import spreadsheets_and_worksheets as gsheets
from eCopernicium.core.sheets_functions import read_and_write as gsheet_data
from eCopernicium.core.sheets_functions import Visualization as visualization
from eCopernicium.core.sheets_functions import interactions
from eCopernicium.core.sheets_functions.requests import *
from eCopernicium.core.aux_functions import colnum_string, get_exchange_rate



def update_companies_and_currencies(service, spreadsheet_id, company_name, company_code, currency, Companies_region_name = 'Companies', Currencies_region_name = 'Currencies', aux_sheet= 'Aux'):
    
    # 1. Update Regions info in "Aux" sheet
    # 1.1. Get Named ranges in "Aux" sheet
    # 1.1.1. Get Aux sheet id
    aux_sheet_id = gsheets.get_sheetId(spreadsheet_id, [aux_sheet])[0]
    # 1.1.2. Get info about named ranges
    named_range_info = gsheets.get_named_range_info(service, spreadsheet_id, aux_sheet)
    
    
    # 1.2. Update info for Companies_region_name regions
    # 1.2.1. Get Companies_region_name region location: intial and last cell
    region_ini_cell = [named_range_info[Companies_region_name]['startRowIndex'] +
                       1, named_range_info[Companies_region_name]['startColumnIndex'] + 1]
    region_end_cell = [named_range_info[Companies_region_name]['endRowIndex'],
                       named_range_info[Companies_region_name]['endColumnIndex']]
    n_companies = region_end_cell[0] - region_ini_cell[0] - 1
    # 1.2.2. Write new companies in Company region in "Aux" sheet
    companies_output = []
    companies_output.append([company_name[i] if i < len(
        company_name) else '' for i in range(n_companies)])
    companies_output.append([company_code[i] if i < len(
        company_name) else '' for i in range(n_companies)])
    companies_output.append([currency[i] if i < len(
        company_name) else '' for i in range(n_companies)])
    gsheet_data.write_ws(service, spreadsheet_id, aux_sheet + '!' + colnum_string(
        region_ini_cell[1]) + str(region_ini_cell[0] + 1), companies_output, dim='COLUMNS')
    
    # 1.2.3. Save info for hide rows with no regions in KPIs named range
    request_body = {'requests': []}
    # Request: unhide KPIs region
    request_body = visualization.apply_unhide(service, spreadsheet_id, aux_sheet_id,
                                region_ini_cell[0] - 1, region_end_cell[0], request_body=request_body, dim="ROWS")
    # Request: hide unnecesary KPIs region rows
    request_body = visualization.apply_hide(service, spreadsheet_id, aux_sheet_id, region_ini_cell[0] + len(
        company_name), region_end_cell[0] - 1, request_body=request_body, dim="ROWS")
    run_batchUpdate_request(service, spreadsheet_id, request_body)
    
    # 2. Update info for currencies
    ## 2.1. Get "Currencies" region location: intial and last cell
    region_ini_cell = [named_range_info[Currencies_region_name]['startRowIndex'] +
                       1, named_range_info[Currencies_region_name]['startColumnIndex'] + 1]
    region_end_cell = [named_range_info[Currencies_region_name]['endRowIndex'],
                       named_range_info[Currencies_region_name]['endColumnIndex']]
    
    # 2.2. Get current currencies and add new ones 
    ### 2.2.1. Get current currencies and the ones to add
    currency_prev_data = gsheet_data.read_ws(service, spreadsheet_id, Currencies_region_name, read_dim='COLUMNS')
    new_currencies = [cur for cur in currency if cur not in currency_prev_data[0]]
    
    ### 2.2.2. Insert rows for add new currencies
    interactions.insert_rows(service, spreadsheet_id, aux_sheet_id, region_end_cell[0], region_end_cell[0] + len(new_currencies))
    
    ### 2.2.3. 
    curr_array = currency_prev_data[0][:-1] + new_currencies + [currency_prev_data[0][-1]]
    exchange_array = ['']
    for i in range(1, len(curr_array) - 1):
        exchange_array.append(get_exchange_rate(curr_array[i]))
    
    gsheet_data.write_ws(service, spreadsheet_id, aux_sheet + '!' + colnum_string(region_ini_cell[1]) + str(region_ini_cell[0]), [curr_array, exchange_array], dim='COLUMNS')

    return