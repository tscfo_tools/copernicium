
import requests
from datetime import date


def output_reporting_name(output_folder, client_name):
    
    return output_folder + "\\" + str(date.today().year)[2:] + client_name + "_Reporting_Forecast.xlsx"


def unique(list1):
      
    # insert the list to the set
    list_set = set(list1)
    # convert the set to the list
    unique_list = (list(list_set))
    x = []
    for x_ in unique_list:
        x.append(x_)
    return x


def get_exchange_rate(base_curr):

    # base currency or reference currency
    out_curr = "EUR"

    # exchange data dates
    start_date = date.today().strftime("%Y-%m-%d")
    end_date = date.today().strftime("%Y-%m-%d")

    # api url for request 
    url = 'https://api.exchangerate.host/timeseries?base={0}&start_date={1}&end_date={2}&symbols={3}'.format(base_curr, start_date, end_date, out_curr)
    response = requests.get(url)

    # retrive response in json format
    data = response.json()
    exchange_rate = data["rates"][start_date][out_curr]
    
    return exchange_rate


def extract_int(strng):
    tmp = ""
    for ele in strng:
        if ele.isdigit():
            tmp += ele
    
    return int(tmp)


def colnum_string(n):
    string = ""
    while n > 0:
        n, remainder = divmod(n - 1, 26)
        string = chr(65 + remainder) + string
    return string



# Color codes conversion
def rgb_to_hex(r, g, b):
  return ('#{:X}{:X}{:X}').format(r, g, b)

def hex_to_rgb(hex):

    hex = hex.strip().replace('#', '')
    rgb = []
    for i in (0, 2, 4):
        decimal = int(hex[i:i+2], 16)
        rgb.append(decimal)

    return rgb