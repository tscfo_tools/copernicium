
import PySimpleGUI as sg
from webbrowser import open as open_browser
from UI.layout import edit_main_layout_eCopernicium as edit_main_layout

from .edit_client.app_management_edit import actions_edit_main_window

# Define theme for all windows
sg.theme('DefaultNoMoreNagging')
sg.set_options(font=('Helvetica', 10))

def open_app(app_name, icon_path):
    
    # Show window for edit client
    second_layout = edit_main_layout()
    main_win = sg.Window('eCopernicium - Edit client', second_layout, auto_size_text = 18, resizable=True, finalize=True)
    
    return main_win


def actions_main_window(main_win):
    
    # Copernicium actions for edit client
    actions_edit_main_window(main_win)
            

    return
