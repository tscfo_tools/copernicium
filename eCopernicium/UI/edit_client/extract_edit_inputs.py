
import win32com.client as win32
from countryinfo import CountryInfo

from eCopernicium.client_secret_account_json import *
from eCopernicium.core.sheets_functions.spreadsheets_and_worksheets import get_spreadsheetId
from eCopernicium.core.sheets_functions.read_and_write import read_ws
from eCopernicium.core.aux_functions import rgb_to_hex

def extract_edit_main_inputs(values):
    class user_inputs:
        def __init__(self, values):
            # Client
            ## Reporting url and id
            self.report_url = values["reporting_url"]
            self.report_id = get_spreadsheetId(values["reporting_url"])
            # Edit options:
            ## Regions
            self.edit_regions = values["edit_regions"]
            ## Companies
            self.edit_companies = values["edit_companies"]
            ## CCs
            self.edit_cc = values["edit_cc"]
            ## Format
            self.edit_format = values["edit_format"]
                       

    return user_inputs(values)

def extract_inputs_from_reporting(service, user_inputs):


    ## 1. Get data from Aux sheet
    ### Get regions data
    if user_inputs.edit_regions:
        region_data = read_ws(service, user_inputs.report_id, 'KPIs_Regions')
        user_inputs.regions = [region_data[i][-1] if len(region_data[i]) > 1 else '' for i in range(1, len(region_data) - 1)]
    ### Get companies data
    if user_inputs.edit_companies:
        comp_data = read_ws(service, user_inputs.report_id, 'Companies')
        user_inputs.comp_name = [comp_data[i][0] if len(comp_data[i]) > 0 else '' for i in range(1, len(comp_data) - 1)]
        user_inputs.comp_code = [comp_data[i][1] if len(comp_data[i]) > 0 else '' for i in range(1, len(comp_data) - 1)]
        user_inputs.country = ["" for i in range(len(user_inputs.comp_code))]
        user_inputs.currency  = [comp_data[i][2] if len(comp_data[i]) > 0 else '' for i in range(1, len(comp_data) - 1)]
    ### Get CCs data
    if user_inputs.edit_cc:
        ### Personnel CC
        cc_pers_data = read_ws(service, user_inputs.report_id, 'CC_Personnel')
        user_inputs.CC_P = [cc_pers_data[i][0] if cc_pers_data[i][0] != 'xxx' else '' for i in range(1, len(cc_pers_data))]
        ### Operating CC
        cc_ops_data = read_ws(service, user_inputs.report_id, 'CC_Operating')
        user_inputs.CC_O = [cc_ops_data[i][0] if cc_ops_data[i][0] != 'xxx' else '' for i in range(1, len(cc_ops_data))]
    ### Get formats
    if user_inputs.edit_format:
        ### Main color -> ws "REPORTING->", cell "B5"
        ### Secondary color -> ws "REPORTING->", cell "B6"
        try:
            # Get ws "REPORTING->!B5:B6"
            # Get all sheet info
            sheet_info = service.spreadsheets().get(
                spreadsheetId=user_inputs.report_id,
                ranges="REPORTING->!B5:B6",
                includeGridData=True
            ).execute()
            
            
            # Main color -> cell "B5"
            bg_cell_rgb_key = sheet_info['sheets'][0]['data'][0]['rowData'][0]['values'][0]['effectiveFormat']['backgroundColorStyle']['rgbColor']
            main_rgb_color = [round(bg_cell_rgb_key['red'] * 255) if 'red' in bg_cell_rgb_key.keys() else 0, \
                                  round(bg_cell_rgb_key['green'] * 255) if 'green' in bg_cell_rgb_key.keys() else 0, \
                                  round(bg_cell_rgb_key['blue'] * 255) if 'blue' in bg_cell_rgb_key.keys() else 0] 

            user_inputs.color_1_old = rgb_to_hex(main_rgb_color[0], main_rgb_color[1], main_rgb_color[2])
            
            # Secondary color -> cell "B6"
            bg_cell_rgb_key = sheet_info['sheets'][0]['data'][0]['rowData'][1]['values'][0]['effectiveFormat']['backgroundColorStyle']['rgbColor']
            sec_rgb_color = [round(bg_cell_rgb_key['red'] * 255) if 'red' in bg_cell_rgb_key.keys() else 0, \
                                  round(bg_cell_rgb_key['green'] * 255) if 'green' in bg_cell_rgb_key.keys() else 0, \
                                  round(bg_cell_rgb_key['blue'] * 255) if 'blue' in bg_cell_rgb_key.keys() else 0] 
                
            user_inputs.color_2_old = rgb_to_hex(sec_rgb_color[0], sec_rgb_color[1], sec_rgb_color[2])
        except:
            # Main color
            user_inputs.color_1_old = ""
            # Secondary color
            user_inputs.color_2_old = ""

    return user_inputs


def extract_edit_inputs_for_reporting(user_inputs, values):

    ## 1.2. Get data
    ### Get regions data
    if user_inputs.edit_regions:
        user_inputs.regions = []
        if values["region_1"] != '': user_inputs.regions.append(values["region_1"])
        if values["region_2"] != '': user_inputs.regions.append(values["region_2"])
        if values["region_3"] != '': user_inputs.regions.append(values["region_3"])
            
    ### Get companies data
    if user_inputs.edit_companies:
        user_inputs.prev_comp_name, user_inputs.prev_comp_code = user_inputs.comp_name, user_inputs.comp_code
        user_inputs.prev_country, user_inputs.prev_currency = user_inputs.country, user_inputs.currency 
        user_inputs.comp_name, user_inputs.comp_code, user_inputs.country, user_inputs.currency = [], [], [], []
        for i in range(1, 5):
            if values["company_name_" + str(i)] != '': 
                user_inputs.comp_name.append(values["company_name_" + str(i)])
                user_inputs.comp_code.append(values["company_code_" + str(i)])
                user_inputs.country.append(values["country_" + str(i)])
                user_inputs.currency.append(CountryInfo(values["country_" + str(i)].lower()).currencies()[0])
            
    ### Get CCs data
    if user_inputs.edit_cc:
        # Get number of Operating CCs before editing
        user_inputs.n_prev_CC_O = len([elm for elm in user_inputs.CC_O if elm != ''])
        #Get new Operating CCs
        user_inputs.CC_P, user_inputs.CC_O = [], []
        for i in range(1, 9):
            ### Personnel costs
            if values["CC_P_" + str(i)] != '': 
                user_inputs.CC_P.append(values["CC_P_" + str(i)])
            ### Operating costs
            if values["CC_O_" + str(i)] != '': 
                user_inputs.CC_O.append(values["CC_O_" + str(i)])

    ### Get formats
    if user_inputs.edit_format:
        #### Colors to replace
        user_inputs.old_color_1 = values["old_color_1"]
        user_inputs.old_color_2 = values['old_color_2']
        #### New colors
        user_inputs.new_color_1 = values["new_color_1"]
        user_inputs.new_color_2 = values['new_color_2']

    return user_inputs
