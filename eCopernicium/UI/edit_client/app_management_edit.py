

from time import time
from sys import exc_info
from webbrowser import open as open_browser
import PySimpleGUI as sg

from UI.layout import edit_secondary_layout
from eCopernicium.UI.edit_client.extract_edit_inputs import extract_edit_main_inputs, extract_inputs_from_reporting, extract_edit_inputs_for_reporting
from eCopernicium.UI.edit_client.edit_inputs_errors import edit_input_errors, edit_input_for_reporting_errors
from eCopernicium.core.copernicium_edit import copernicium

from eCopernicium.client_secret_account_json import *
from eCopernicium.core.sheets_functions.google_apis import Create_Service

def actions_edit_main_window(main_win):
    
    while True:
        event, values = main_win.read()

        if event == sg.WIN_CLOSED:
            break
        elif event == "Cancel":
            main_win.close()
            break

        elif event.startswith("URL "):
            url = event.split(' ')[1]
            open_browser(url)

        elif event =="submit":
            
            # extract user inputs from main window
            user_inputs = extract_edit_main_inputs(values)
            
            # function with errors -> next step
            error_status = edit_input_errors(user_inputs)

            # Show second window
            if error_status.input_error == False:
                
                # Create service for Google sheets
                service = Create_Service(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES)
                
                # Extract current reporting parameters
                user_inputs = extract_inputs_from_reporting(service, user_inputs)
                # Show second window for edit client
                second_layout = edit_secondary_layout(user_inputs)
                new_win = sg.Window('eCopernicium - Edit client', second_layout, auto_size_text = 18, resizable=True, finalize=True) # , size=(570, 650)) #, icon="tsc.ico", finalize=True, element_justification='c')
                # Copernicium actions for new client
                actions_edit_secondary_window(service, new_win, user_inputs)


    return


def actions_edit_secondary_window(service, main_win, user_inputs):
    
    while True:
        event, values = main_win.read()

        if event == sg.WIN_CLOSED:
            break
        elif event == "Cancel":
            main_win.close()
            break

        elif event.startswith("URL "):
            url = event.split(' ')[1]
            open_browser(url)

        elif event =="submit":
            
            # extract user inputs from main window
            user_inputs = extract_edit_inputs_for_reporting(user_inputs, values)
            
            # function with errors -> next step
            error_status = edit_input_for_reporting_errors(user_inputs)

             # Run Copernicium
            if error_status.input_error == False:
                try:
                    ## Start time control
                    t_ini = time()
                    sum_up_msg = ""

                    ## Run Copernicium
                    copernicium(service, user_inputs)
                    
                    ## End time control
                    t_end = time()
                    time_msg = sum_up_msg + "\nTime: " + str(round(t_end - t_ini, 2)) + " sec."
                    sg.Popup(time_msg)
                except:
                    sg.Popup("Error", exc_info()[1])
                
                # Close window
                main_win.close()


    return
