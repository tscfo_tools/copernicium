
import PySimpleGUI as sg
from webbrowser import open as open_browser
from UI.layout import select_action_layout, new_layout, edit_main_layout_Copernicium as edit_main_layout

from .new_client.app_management_new import actions_new_window
from .edit_client.app_management_edit import actions_edit_main_window

# Define theme for all windows
sg.theme('DefaultNoMoreNagging') 
sg.set_options(font=('Helvetica', 10))

def open_app(app_name, icon_path):
    
    layout = select_action_layout()
    main_win = sg.Window(app_name, layout, auto_size_text = 18, resizable=True, finalize=True) #  icon=icon_path, size=(570, 650))
    main_win.normal()
    
    return main_win


def actions_main_window(main_win):
    
    while True:
        event, values = main_win.read()

        if event == sg.WIN_CLOSED:
            break
        elif event == "Cancel":
            main_win.close()
            break

        elif event.startswith("URL "):
            url = event.split(' ')[1]
            open_browser(url)

        elif event == "new_client":
            
            # Show window for new client
            second_layout = new_layout()
            new_win = sg.Window('Copernicium - New client', second_layout, auto_size_text = 18, resizable=True, finalize=True, size=(570, 650)) #, icon="tsc.ico", finalize=True, element_justification='c')
            # Copernicium actions for new client
            actions_new_window(new_win)

        elif event == "edit_client":
            
            # Show window for edit client
            second_layout = edit_main_layout()
            new_win = sg.Window('Copernicium - Edit client', second_layout, auto_size_text = 18, resizable=True, finalize=True) # , size=(570, 650)) #, icon="tsc.ico", finalize=True, element_justification='c')
            # Copernicium actions for new client
            actions_edit_main_window(new_win)
            

    return
