
import PySimpleGUI as sg


def edit_input_errors(inputs):
    class input_error_status:
        def __init__(self, inputs):

            # Intialize error msg as False (True if input error)
            self.input_error = False

            self.error_msg = 'Select:'
            # Cannot be empty
            ## Client name
            if inputs.report_path == '':
                self.input_error = True
                self.error_msg += '\n - Reporting'
            ## At least one edition field
            if inputs.edit_regions == False and inputs.edit_companies == False and inputs.edit_cc == False and inputs.edit_format == False:
                self.input_error = True
                self.error_msg += '\n - At least one field to edit'

            # Popup message if not all inputs
            if self.input_error:
                sg.Popup(self.error_msg)

    return input_error_status(inputs)


def edit_input_for_reporting_errors(inputs):

    class input_error_status:
        def __init__(self, inputs):

            # Intialize error msg as False (True if input error)
            self.input_error = False

            self.error_msg = 'Select:'
            # Cannot be empty
            ## Client name
            if inputs.edit_format:
                if inputs.old_color_1 == '' or inputs.old_color_2 == '' or inputs.new_color_1 == '' or inputs.new_color_2 == '':
                    self.input_error = True
                    self.error_msg += '\n - Format: Fill all color fields'

            ## At least one edition field
            if inputs.edit_companies:
                if len(inputs.comp_name) > len([elm for elm in inputs.country if elm != '']):
                    self.input_error = True
                    self.error_msg += '\n - Country fields are empty.'
            

            # Popup message if not all inputs
            if self.input_error:
                sg.Popup(self.error_msg)

    return input_error_status(inputs)