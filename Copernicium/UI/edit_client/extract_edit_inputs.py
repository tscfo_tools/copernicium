
import win32com.client as win32
from countryinfo import CountryInfo

from Copernicium.core.excel_functions import dec_to_hex_color


def extract_edit_main_inputs(values):
    class user_inputs:
        def __init__(self, values):
            # Client
            ## Reporting path
            self.report_path = values["reporting_path"]
            # Edit options:
            ## Regions
            self.edit_regions = values["edit_regions"]
            ## Companies
            self.edit_companies = values["edit_companies"]
            ## CCs
            self.edit_cc = values["edit_cc"]
            ## Format
            self.edit_format = values["edit_format"]
                       

    return user_inputs(values)

def extract_inputs_from_reporting(user_inputs):

    # Open excel
    # 0. Open wb
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    excel.Visible = False
    excel.DisplayAlerts = False
    wb_inp = excel.Workbooks.Open(user_inputs.report_path)

    # 1. Get data from Aux worksheet
    ## 1.1. Call Aux ws
    ws_aux = wb_inp.Sheets("Aux")

    ## 1.2. Get data
    ### Get regions data
    if user_inputs.edit_regions:
        region_data = ws_aux.Range("KPIs_Regions").Value
        user_inputs.regions = [region_data[i][-1] if region_data[i][-1] != None else '' for i in range(1, len(region_data) - 1)]
    ### Get companies data
    if user_inputs.edit_companies:
        comp_data = ws_aux.Range("Companies").Value
        user_inputs.comp_name = [comp_data[i][0] if comp_data[i][0] != None else '' for i in range(1, len(comp_data) - 1)]
        user_inputs.comp_code = [comp_data[i][1] if comp_data[i][1] != None else '' for i in range(1, len(comp_data) - 1)]
        user_inputs.country = ["" for i in range(len(user_inputs.comp_code))] # [comp_data[i][2] if comp_data[i][2] != None else '' for i in range(1, len(comp_data) - 1)]
        user_inputs.currency  = [comp_data[i][2] if comp_data[i][2] != None else '' for i in range(1, len(comp_data) - 1)]
    ### Get CCs data
    if user_inputs.edit_cc:
        #### Personnel CC
        cc_pers_data = ws_aux.Range("CC_Personnel").Value
        user_inputs.CC_P = [cc_pers_data[i][0] if cc_pers_data[i][0] != 'xxx' else '' for i in range(1, len(cc_pers_data))]
        #### Operating CC
        cc_ops_data = ws_aux.Range("CC_Operating").Value
        user_inputs.CC_O = [cc_ops_data[i][0] if cc_ops_data[i][0] != 'xxx' else '' for i in range(1, len(cc_ops_data))]
    ### Get formats
    if user_inputs.edit_format:
        #### Main color -> ws "REPORTING->", cell "B5"
        #### Secondary color -> ws "REPORTING->", cell "B6"
        try:
            # Get ws "REPORTING->"
            ws_reporting = wb_inp.Sheets("REPORTING->")
            # Main color
            user_inputs.color_1_old = dec_to_hex_color(ws_reporting.Range("B5").Interior.Color)
            # Secondary color
            user_inputs.color_2_old = dec_to_hex_color(ws_reporting.Range("B6").Interior.Color)
        except:
            # Main color
            user_inputs.color_1_old = ""
            # Secondary color
            user_inputs.color_2_old = ""

    # Close wb
    wb_inp.Close(True)
    excel.Visible = False 
    excel.DisplayAlerts = False
    excel.Application.Quit()

    return user_inputs


def extract_edit_inputs_for_reporting(user_inputs, values):

    ## 1.2. Get data
    ### Get regions data
    if user_inputs.edit_regions:
        user_inputs.regions = []
        if values["region_1"] != '': user_inputs.regions.append(values["region_1"])
        if values["region_2"] != '': user_inputs.regions.append(values["region_2"])
        if values["region_3"] != '': user_inputs.regions.append(values["region_3"])
            
    ### Get companies data
    if user_inputs.edit_companies:
        user_inputs.prev_comp_name, user_inputs.prev_comp_code = user_inputs.comp_name, user_inputs.comp_code
        user_inputs.prev_country, user_inputs.prev_currency = user_inputs.country, user_inputs.currency 
        user_inputs.comp_name, user_inputs.comp_code, user_inputs.country, user_inputs.currency = [], [], [], []
        for i in range(1, 5):
            if values["company_name_" + str(i)] != '': 
                user_inputs.comp_name.append(values["company_name_" + str(i)])
                user_inputs.comp_code.append(values["company_code_" + str(i)])
                user_inputs.country.append(values["country_" + str(i)])
                user_inputs.currency.append(CountryInfo(values["country_" + str(i)].lower()).currencies()[0])
            
    ### Get CCs data
    if user_inputs.edit_cc:
        # Get number of Operating CCs before editing
        user_inputs.n_prev_CC_O = len([elm for elm in user_inputs.CC_O if elm != ''])
        #Get new Operating CCs
        user_inputs.CC_P, user_inputs.CC_O = [], []
        for i in range(1, 9):
            ### Personnel costs
            if values["CC_P_" + str(i)] != '': 
                user_inputs.CC_P.append(values["CC_P_" + str(i)])
            ### Operating costs
            if values["CC_O_" + str(i)] != '': 
                user_inputs.CC_O.append(values["CC_O_" + str(i)])

    ### Get formats
    if user_inputs.edit_format:
        #### Colors to replace
        user_inputs.old_color_1 = values["old_color_1"]
        user_inputs.old_color_2 = values['old_color_2']
        #### New colors
        user_inputs.new_color_1 = values["new_color_1"]
        user_inputs.new_color_2 = values['new_color_2']

    return user_inputs
