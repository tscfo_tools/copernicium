
from countryinfo import CountryInfo

def extract_new_inputs(values):
    class user_inputs:
        def __init__(self, values):
            # Client
            ## Client name
            self.client_name = values["client_name"]
            ## Regions
            self.region = []
            if values["region_1"] != '': self.region.append(values["region_1"])
            if values["region_2"] != '': self.region.append(values["region_2"])
            if values["region_3"] != '': self.region.append(values["region_3"])
            ## Companies: name, code & currency
            self.comp_name, self.comp_code, self.country, self.currency = [], [], [], []
            for i in range(1, 5):
                if values["company_name_" + str(i)] != '': 
                    self.comp_name.append(values["company_name_" + str(i)])
                    self.comp_code.append(values["company_code_" + str(i)])
                    self.country.append(values["country_" + str(i)])
                    self.currency.append(CountryInfo(values["country_" + str(i)].lower()).currencies()[0])
            ## CCs: Personnel & Operating costs
            self.CC_P, self.CC_O = [], []
            for i in range(1, 9):
                ### Personnel costs
                if values["CC_P_" + str(i)] != '': 
                    self.CC_P.append(values["CC_P_" + str(i)])
                ### Operating costs
                if values["CC_O_" + str(i)] != '': 
                    self.CC_O.append(values["CC_O_" + str(i)])
                    
            # Format
            self.color_1_input = values["color_1"]
            self.color_2_input = values['color_2']

            # Base model path
            self.model_path = values["model_path"]
            # Output folder 
            self.output_folder = values["output_folder"]
            

    return user_inputs(values)