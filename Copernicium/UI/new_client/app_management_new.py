

from time import time
from sys import exc_info
from webbrowser import open as open_browser
import PySimpleGUI as sg

from Copernicium.UI.new_client.extract_new_inputs import extract_new_inputs
from Copernicium.UI.new_client.new_inputs_errors import new_input_errors
from Copernicium.core.copernicium_new import copernicium


def actions_new_window(main_win):
    
    while True:
        event, values = main_win.read()

        if event == sg.WIN_CLOSED:
            break
        elif event == "Cancel":
            main_win.close()
            break

        elif event =="submit":
            
            # extract user inputs from main window
            user_inputs = extract_new_inputs(values)
            
            # function with errors -> next step
            error_status = new_input_errors(user_inputs)

            # Run Copernicium
            if error_status.input_error == False:
                try:
                    ## Start time control
                    t_ini = time()
                    sum_up_msg = ""

                    ## Run Copernicium
                    copernicium(user_inputs.client_name, user_inputs.region, user_inputs.comp_name, user_inputs.comp_code, user_inputs.currency, user_inputs.CC_P, user_inputs.CC_O, user_inputs.color_1_input, user_inputs.color_2_input, user_inputs.model_path, user_inputs.output_folder)
                    
                    ## End time control
                    t_end = time()
                    time_msg = sum_up_msg + "\nTime: " + str(round(t_end - t_ini, 2)) + " sec."
                    sg.Popup(time_msg)

                    # Close window
                    main_win.close()
                    
                except:
                    sg.Popup("Error", exc_info()[1])

        elif event.startswith("URL "):
            url = event.split(' ')[1]
            open_browser(url)

    return