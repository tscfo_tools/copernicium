
import PySimpleGUI as sg

def new_input_errors(inputs):
    class input_error_status:
        def __init__(self, inputs):

            # Intialize error msg as False (True if input error)
            self.input_error = False

            self.error_msg = 'Select:'
            # Cannot be empty
            ## Client name
            if inputs.client_name == '':
                self.input_error = True
                self.error_msg += '\n - Client name'
            ## Main color
            if inputs.color_1_input == '':
                self.input_error = True
                self.error_msg += '\n - Main color'
            ## Secondary color
            if inputs.color_2_input == '':
                self.input_error = True
                self.error_msg += '\n - Secondary color'
            ## Base model
            if inputs.model_path == '':
                self.input_error = True
                self.error_msg += '\n - Base model'
            ## Output folder
            if inputs.output_folder == '':
                self.input_error = True
                self.error_msg += '\n - Output folder'
            ## Company information if companies have been declared
            if "" in inputs.comp_code:
                self.input_error = True
                self.error_msg += '\n - Company code'
            
            # Popup message if not all inputs
            if self.input_error:
                sg.Popup(self.error_msg)

    return input_error_status(inputs)