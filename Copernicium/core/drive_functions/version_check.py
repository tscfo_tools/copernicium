

from .client_secret_account_json import *
from .google_apis import Create_Service 
from .drive_functions import *


def get_version(version_string):
    file_version = version_string.split(".")
    if len(file_version) == 3: int_version = int(version_string.replace(".", ""))
    if len(file_version) == 2: int_version = int(version_string.replace(".", "") + "0")
    if len(file_version) == 1: int_version = int(version_string.replace(".", "") + "00")
    if len(file_version) == 0: int_version = 0

    return int_version


def check_if_new_version(updated_folder_id, tool_name, current_version_str): 
    
    API_NAME = 'drive'
    API_VERSION = 'v3'
    SCOPES = ['https://www.googleapis.com/auth/drive']

    try:
        # Connect with google drive api
        drive_service = Create_Service(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES)
        
        # Get list of files in "Updated tools" folder
        file_list = list_files(drive_service, updated_folder_id)[0] 
        ## Get tool files in folder
        tool_files = [file_list[i] for i in range(len(file_list)) if file_list[i].startswith(tool_name + "_v")]
        tool_files_version = [tool_files[i].replace(tool_name + "_v", "").replace(".exe", "") for i in range(len(tool_files))]
        ## Get version of these files
        int_versions = [get_version(tool_files_version[i]) for i in range(len(tool_files_version))]
        ### Latest version available
        latest_version_avaliable = max(int_versions)
        
        # Get current version int
        current_version = get_version(current_version_str)


        # Check if there is a new version
        if latest_version_avaliable > current_version:
            return True
        else:
            return False
    except:
        return False
    



