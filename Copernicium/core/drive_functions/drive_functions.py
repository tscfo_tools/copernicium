

#########################################
# Get all files and folders in a shared folder
#########################################

def list_files(service, folder_id):
    try:
        query = f"parents = '{folder_id}'"
        
        response = service.files().list(q=query).execute()
        files = response.get('files')
        nextPageToken = response.get('nextPageToken')
        
        while nextPageToken:
            response = service.files().list(q=query).execute()
            files.extend(response.get('files'))
            nextPageToken = response.get('nextPageToken')
        
        files_name = [files[i]['name'] for i in range(len(files))]
        files_id = [files[i]['id'] for i in range(len(files))]
        files_type = [files[i]['mimeType'].split('.')[-1] for i in range(len(files))]
        
        return files_name, files_id, files_type
    
    except Exception as e:
        print(e)
        return None
