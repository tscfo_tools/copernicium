
import win32com.client as win32

from Copernicium.core.aux_functions import *
from Copernicium.core.excel_functions import *
from Copernicium.core.update_functions.update_CC import *
from Copernicium.core.update_functions.update_company import *
from Copernicium.core.update_functions.update_currency import *
from Copernicium.core.update_functions.update_format import *
from Copernicium.core.update_functions.update_region import *


def copernicium(user_inputs):

    # 0. Open wb
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    excel.Visible = False
    excel.DisplayAlerts = False
    wb_inp = excel.Workbooks.Open(user_inputs.report_path)

    # 1. Set ws to update forecast
    ## 1.1. Get all worksheet names in the spreadsheet
    ws_list = [sheet.Name for sheet in wb_inp.Sheets]
 

    ##############################################
    ## Edit section 
    ##############################################
    ####################################
    # Call Aux worksheet
    if user_inputs.edit_regions or user_inputs.edit_companies or user_inputs.edit_cc:
        ws_aux = wb_inp.Sheets("Aux")
    
    if user_inputs.edit_regions:
        # Update region first for unhiding region worksheets
        ## Hiding unnecesary worksheets is done at the end because of files management
        update_region_data(wb_inp, ws_aux, user_inputs.regions)


    if user_inputs.edit_format:
        # Update format
        ### Main & Secondary color
        color_1_old, color_2_old = hex_to_dec_color(user_inputs.old_color_1), hex_to_dec_color(user_inputs.old_color_2)
        color_1_new, color_2_new = hex_to_dec_color(user_inputs.new_color_1), hex_to_dec_color(user_inputs.new_color_2)
        ## Update
        update_format(excel, wb_inp, ws_list, color_1_old, color_1_new, color_2_old, color_2_new)
            
    

    if user_inputs.edit_companies:
        ## Edit companies
        update_companies(ws_aux, user_inputs.comp_name, user_inputs.comp_code, user_inputs.currency)
        ## Update currencies
        update_currency(ws_aux, user_inputs.currency)

    if user_inputs.edit_cc:
        ## Get client name
        client_name = ws_aux.Range("client").Value
        ## Edit CCs
        update_CC(wb_inp, ws_list, ws_aux, user_inputs.CC_P, user_inputs.CC_O, client_name, user_inputs.n_prev_CC_O)

    if user_inputs.edit_regions:
        ## Edit regions
        output_file_path = user_inputs.report_path.replace("/","\\")
        output_file_path = output_file_path.replace("." + output_file_path.split(".")[-1], "_edit." + output_file_path.split(".")[-1])
        modify_region_ws(excel, wb_inp, output_file_path, user_inputs.regions)
    else:
        ## If not edit regions, Save and close as *_edit.*
        output_file_path = user_inputs.report_path.replace("/","\\")
        output_file_path = output_file_path.replace("." + output_file_path.split(".")[-1], "_edit." + output_file_path.split(".")[-1])
        ## Close and quit excel
        wb_inp.SaveAs(Filename= output_file_path)
        wb_inp.Close(True)
        excel.Visible = False 
        excel.DisplayAlerts = False
        excel.Application.Quit()

    return

