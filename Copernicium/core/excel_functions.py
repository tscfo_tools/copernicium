

def hex_to_dec_color(hex_color):
    hex_color = hex_color[hex_color.index("#"):]
    return int((hex_color[5:] + hex_color[3:5] + hex_color[1:3]).lower(), 16)

def dec_to_hex_color(dec_color):
    dec_color = hex(int(dec_color)).replace("0x","#").upper()
    return dec_color[0] + dec_color[5:] + dec_color[3:5] + dec_color[1:3]

def colnum_string(n):
    string = ""
    while n > 0:
        n, remainder = divmod(n - 1, 26)
        string = chr(65 + remainder) + string
    return string