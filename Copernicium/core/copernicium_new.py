
import win32com.client as win32

from Copernicium.core.aux_functions import *
from Copernicium.core.excel_functions import *
from Copernicium.core.update_functions.update_CC import *
from Copernicium.core.update_functions.update_company import *
from Copernicium.core.update_functions.update_currency import *
from Copernicium.core.update_functions.update_format import *
from Copernicium.core.update_functions.update_region import *


def copernicium(client_name, region, comp_name, comp_code, currency, CC_P, CC_O, color_1, color_2, model_path, output_folder_path):
    """
        Customize base model excel. Actions:
            + Client name
            + Format - Main and secondary colors:
                · Interior cells
                · Title font
            + Regions: Change region names and hide all unnecesary sheets.
            + Companies: company name, code and currency (country)
            + Currencies: add the new currencies (and highlight new currency exchange cell for being completed by user)
            + CCs: Operating and Personnel costs
    """

    # 0. Open wb
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    excel.Visible = False
    excel.DisplayAlerts = False
    wb_inp = excel.Workbooks.Open(model_path)

    # 1. Set ws to update forecast
    ## 1.1. Get all worksheet names in the spreadsheet
    ws_list = [sheet.Name for sheet in wb_inp.Sheets]
 

    ##############################################
    ## Update section 
    ##############################################
    # Update format
    ### Main & Secondary color
    color_1_old, color_2_old = 2433710, 11522302
    color_1_new, color_2_new = hex_to_dec_color(color_1), hex_to_dec_color(color_2)
    ## Update
    update_format(excel, wb_inp, ws_list, color_1_old, color_1_new, color_2_old, color_2_new)
            
    ####################################
    # Update data in Aux worksheet
    ## Call Aux ws
    ws_aux = wb_inp.Sheets("Aux")

    ## Client name
    ws_aux.Range("client").Value = client_name

    ## Companies
    update_companies(ws_aux, comp_name, comp_code, currency)

    ## Currencies
    update_currency(ws_aux, currency)

    ## CCs
    update_CC(wb_inp, ws_list, ws_aux, CC_P, CC_O, client_name)

    ## Regions
    output_file_path = output_reporting_name(output_folder_path.replace("/","\\"), client_name)
    update_region_data(wb_inp, ws_aux, region)
    modify_region_ws(excel, wb_inp, output_file_path, region)

    return

