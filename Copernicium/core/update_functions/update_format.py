
import win32com.client as win32

def update_format(excel, wb_inp, ws_list, color_1_old, color_1_new, color_2_old, color_2_new):
    
    for ws_name in ws_list:
        # Call worksheet
        ws_inp = wb_inp.Sheets(ws_name)
        
        # If worksheet is hidden -> Go to next
        if ws_inp.Visible == win32.constants.xlSheetVisible:
            
            # Replace tab color
            ## Main color
            if ws_inp.Tab.Color == color_1_old:
                ws_inp.Tab.Color = color_1_new
            ## Secondary color
            if ws_inp.Tab.Color == color_2_old:
                ws_inp.Tab.Color = color_2_new
            
            
            # Replace cells interior color
            ## Main color
            excel.FindFormat.Clear
            excel.ReplaceFormat.Clear
            excel.FindFormat.Interior.Color = color_1_old
            excel.ReplaceFormat.Interior.Color = color_1_new
            
            ws_inp.Cells.Replace(What = "", Replacement = "", LookAt = win32.constants.xlPart, \
                    SearchOrder = win32.constants.xlByRows, MatchCase = False, \
                    SearchFormat = True, ReplaceFormat = True, FormulaVersion = win32.constants.xlReplaceFormula2)
            
            ## Secondary color
            excel.FindFormat.Clear
            excel.ReplaceFormat.Clear
            excel.FindFormat.Interior.Color = color_2_old
            excel.ReplaceFormat.Interior.Color = color_2_new
            
            ws_inp.Cells.Replace(What = "", Replacement = "", LookAt = win32.constants.xlPart, \
                    SearchOrder = win32.constants.xlByRows, MatchCase = False, \
                    SearchFormat = True, ReplaceFormat = True, FormulaVersion = win32.constants.xlReplaceFormula2)
                          
           
            
            # Replace font color
            # Replace cells interior color
            for i in range(1, min(4, ws_inp.UsedRange.Rows.Count + 1)):
                for j in range(1, min(15, ws_inp.UsedRange.Columns.Count + 1)):
                    # Replace font color
                    ## Main color
                    if ws_inp.Cells(i, j).Font.Color == color_1_old:
                        ws_inp.Cells(i, j).Font.Color = color_1_new
                    ## Secondary color
                    if ws_inp.Cells(i, j).Font.Color == color_2_old:
                        ws_inp.Cells(i, j).Font.Color = color_2_new
    
            
            # Replace figure background colors
            for fig in ws_inp.Shapes:
                if fig.Fill.ForeColor.RGB == color_1_old:
                    fig.Fill.ForeColor.RGB = color_1_new
                
    return
