
from Copernicium.core.excel_functions import colnum_string
from Copernicium.core.aux_functions import unique, get_exchange_rate

def update_currency(ws_aux, currency):
    # Finding table data and location in Aux ws
    cur_ini_cell = [ws_aux.Range("Currencies").Row, ws_aux.Range("Currencies").Column]
    cur_data = ws_aux.Range("Currencies").Value
    cur_array = [cur_data[j][0] for j in range(1, len(cur_data) - 1)]
    
    unique_currency = unique(currency)
    new_cur_input = [unique_currency[i] for i in range(len(unique_currency)) if (unique_currency[i] not in cur_array)]
    
    # Adding new currencies to "Currencies" range
    if len(new_cur_input) > 0:
        ## Insert rows for new currency
        new_currency_ini = cur_ini_cell[0] + len(cur_data) - 1
        new_currency_end = new_currency_ini + (len(new_cur_input)-1)
        ws_aux.Range(str(new_currency_ini) + ":" + str(new_currency_end)).EntireRow.Insert()
        ## Write new currency
        ws_aux.Range(colnum_string(cur_ini_cell[1]) + str(new_currency_ini) + ":" + colnum_string(cur_ini_cell[1]) + str(new_currency_end)).Value = tuple([(new_cur_input[i],) for i in range(len(new_cur_input))])

    # Update exchange rate
    new_cur_data = ws_aux.Range("Currencies").Value    
    new_cur_array = [new_cur_data[j][0] for j in range(1, len(new_cur_data) - 1)]    
        
    current_row = cur_ini_cell[0]
    for i in range(len(new_cur_array)):
        current_row += 1
        try:
            ws_aux.Cells(current_row, cur_ini_cell[1] + 1).Value = get_exchange_rate(new_cur_array[i])
        except:    
            ## If currency not found, highlight cell
            ws_aux.Cells(current_row, cur_ini_cell[1] + 1).Interior.Color = 65535
            
    return