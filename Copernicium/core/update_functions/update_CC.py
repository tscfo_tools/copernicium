
import win32com.client as win32

from Copernicium.core.excel_functions import colnum_string
from Copernicium.core.aux_functions import extract_int


def hide_xxx_rows_OC(wb_inp, CC_op, ws_name, metric_word_col_ref, n_prev_OC):
    """
        wb_inp: wb object
        ws_list: list of worksheet names
        ws_name: ws name where rows with "xxx" are going to be hidden
        metric_word_col_ref: refernce word for finding column where metrics name are
        n_prev_OC: number of Operating cost centers before updating
    """

    ### Find column w/ Metric names -> Look for "Operating costs"
    current_ws = wb_inp.Sheets(ws_name)

    metric_col = current_ws.UsedRange.Find(metric_word_col_ref, LookIn = win32.constants.xlValues, LookAt = win32.constants.xlWhole, \
                    SearchOrder = win32.constants.xlByRows, MatchCase = False).Column
    ### In metrics name col, look for cells with "xxx"s.
    #### Get metrics name
    metric_list = current_ws.Range(colnum_string(metric_col) + "1:" + colnum_string(metric_col) + str(current_ws.UsedRange.Rows.Count)).Value
    metric_list = [metric_list[i][0] for i in range(len(metric_list))]
    
    #### Unhide row
    current_ws.Rows.EntireRow.Hidden = False

    #### If "xxx"s, look color is black or inside grey scale (not blue)
    for i in range(len(metric_list)):
        if metric_list[i] == "xxx":
            # Get font color
            dec_color = current_ws.Cells(i + 1, metric_col).Font.Color
            # Tranform color code: decimal to rgb
            rgb_color = [int(dec_color / (256*256))/255, int((dec_color / 256) % 256)/255, int(dec_color % 256)/255]
            
            # Get cell interior color 
            interior_dec_color = current_ws.Cells(i + 1, metric_col).Interior.Color
            # if font color is black or grey & cell interior blank, hide row
            if (interior_dec_color == 16777215) and ((rgb_color[2] <= 244/255) and abs(rgb_color[1] - rgb_color[2]) < 10/255 and (rgb_color[0] - rgb_color[2]) < 10/255 and (rgb_color[0] - rgb_color[1]) < 10/255):
                current_ws.Rows(str(i + 1)).EntireRow.Hidden = True

    ### If Operating CCs != n_prev_OC -> Look for section "OPERATING COSTS" btw cells w/ "OPERATING COSTS" and "OTHER RESULTS"
    if len(CC_op) != max(5, n_prev_OC):
        OC_section_rows = [current_ws.UsedRange.Find("OPERATING COSTS", LookIn = win32.constants.xlValues, LookAt = win32.constants.xlWhole, \
                        SearchOrder = win32.constants.xlByRows, MatchCase = True).Row, \
                            current_ws.UsedRange.Find("OTHER RESULTS", LookIn = win32.constants.xlValues, LookAt = win32.constants.xlWhole, \
                        SearchOrder = win32.constants.xlByRows, MatchCase = True).Row - 1]

        #### If Operating CCs < n_prev_OC -> Hide rows for CCs that does not exists and are represented as "xxx" (cell interior not white)
        if len(CC_op) < max(5, n_prev_OC):
            for i in range(OC_section_rows[0] - 1, OC_section_rows[1]):
                if metric_list[i] == "xxx":
                    if current_ws.Cells(i + 1, metric_col).Interior.Color != 16777215:
                        current_ws.Rows(str(i + 1) + ":" + str(OC_section_rows[1])).EntireRow.Hidden = True
                        break
        #### If Operating CCs > n_prev_OC -> Add new CC region for new Operating cost CC
        else:
            for i in range(OC_section_rows[0] - 1, OC_section_rows[1]):
                #### Find (max(5, n_prev_OC))th Operating CC (cell interior is not white)
                if metric_list[i] == CC_op[max(5, n_prev_OC) - 1]:
                    if current_ws.Cells(i + 1, metric_col).Interior.Color != 16777215:
                        formula_offset = len(CC_op) - n_prev_OC
                        for j in range(len(CC_op) - n_prev_OC):
                            ##### Insert empty rows for copy new CC section
                            current_ws.Range(str(OC_section_rows[1] + 1) + ":" + str(2 * OC_section_rows[1] + 1 - (i + 1))).Insert()
                            ##### Copy and Paste CC section
                            current_ws.Range(str(i + 1) + ":" + str(OC_section_rows[1])).Copy()
                            current_ws.Range(str(OC_section_rows[1] + 1) + ":" + str(2 * OC_section_rows[1] - (i + 1))).PasteSpecial()
                            
                            ##### Copy formula for CC title for new section
                            current_ws.Cells(str(OC_section_rows[1] + 1), metric_col).Formula = current_ws.Cells(i + 1, metric_col).Formula.replace(str(extract_int(current_ws.Cells(i + 1, metric_col).Formula)), str(extract_int(current_ws.Cells(i + 1, metric_col).Formula) + formula_offset))
                            formula_offset -= 1
                        break

    return


def hide_xxx_rows_PC(wb_inp, ws_name, metric_word_col_ref):
    """
        wb_inp: wb object
        ws_list: list of worksheet names
        ws_name: ws name where rows with "xxx" are going to be hidden
        metric_word_col_ref: refernce word for finding column where metrics name are
    """

    ### Find column w/ Metric names -> Look for "Operating costs"
    current_ws = wb_inp.Sheets(ws_name)
    
    metric_col = current_ws.UsedRange.Find(metric_word_col_ref, LookIn = win32.constants.xlValues, LookAt = win32.constants.xlWhole, \
                    SearchOrder = win32.constants.xlByRows, MatchCase = False).Column
    ### In metrics name col, look for cells with "xxx"s.
    #### Get metrics name
    metric_list = current_ws.Range(colnum_string(metric_col) + "1:" + colnum_string(metric_col) + str(current_ws.UsedRange.Rows.Count)).Value
    metric_list = [metric_list[i][0] for i in range(len(metric_list))]
    
    #### Unhide row
    current_ws.Rows.EntireRow.Hidden = False

    for i in range(len(metric_list)):

        #### If "xxx"s, look color is black or inside grey scale (not blue)
        if metric_list[i] == "xxx":
            # Get font color
            dec_color = current_ws.Cells(i + 1, metric_col).Font.Color
            # Tranform color code: decimal to rgb
            rgb_color = [int(dec_color / (256*256))/255, int((dec_color / 256) % 256)/255, int(dec_color % 256)/255]
            
            # if font color is black or grey, hide row
            if (rgb_color[2] <= 244/255) and abs(rgb_color[1] - rgb_color[2]) < 10/255 and (rgb_color[0] - rgb_color[2]) < 10/255 and (rgb_color[0] - rgb_color[1]) < 10/255:
                current_ws.Rows(str(i + 1)).EntireRow.Hidden = True

    #### Group all groups
    current_ws.Outline.ShowLevels(RowLevels = 1)

    return

def hide_xxx_rows_PL(wb_inp, ws_name, metric_word_col_ref):
    """
        wb_inp: wb object
        ws_list: list of worksheet names
        ws_name: ws name where rows with "xxx" are going to be hidden
        metric_word_col_ref: refernce word for finding column where metrics name are
    """

    ### Find column w/ Metric names -> Look for "Operating costs"
    current_ws = wb_inp.Sheets(ws_name)
    #### Show all groups -> for allowing hide rows
    # current_ws.Outline.ShowLevels(RowLevels = 2)
    
    metric_col = current_ws.UsedRange.Find(metric_word_col_ref, LookIn = win32.constants.xlValues, LookAt = win32.constants.xlPart, \
                    SearchOrder = win32.constants.xlByRows, MatchCase = False).Column
    ### In metrics name col, look for cells with "xxx"s
    #### Get metrics name
    metric_list = current_ws.Range(colnum_string(metric_col) + "1:" + colnum_string(metric_col) + str(current_ws.UsedRange.Rows.Count)).Value
    metric_list = [metric_list[i][0] for i in range(len(metric_list))]
    
    
    ### Find blocks of "Personnel costs" and "Operational costs"
    PC_ranges_ini = [i for i in range(len(metric_list)) if metric_list[i] == "Personnel costs"]
    PC_ranges_ini = PC_ranges_ini + [i for i in range(len(metric_list)) if metric_list[i] == "Operating costs"]
    PC_ranges_end = [(PC_ranges_ini[i] + metric_list[PC_ranges_ini[i]:].index(None)) for i in range(len(PC_ranges_ini))]
    
    #### Look for "xxx"s in each block, and if exists -> hide the row
    for i in range(len(PC_ranges_ini)):
        ##### Unhide rows in PL ranges
        current_ws.Rows(str(PC_ranges_ini[i]) + ":" + str(PC_ranges_end[i])).EntireRow.Hidden = False
        ##### Look for "xxx"s in each block
        rows_to_hide = [(j + 1) for j in range(PC_ranges_ini[i], PC_ranges_end[i]) if metric_list[j] == "xxx"]
        
        if len(rows_to_hide) > 0:
            for j in range(len(rows_to_hide)):
                current_ws.Rows(str(rows_to_hide[j])).EntireRow.Hidden = True
    
    #### Group all groups
    current_ws.Outline.ShowLevels(RowLevels = 1)

    return

def update_CCs_in_Aux(ws_aux, range_name, CC_input):
    # CCs - Personnel costs
    ## Get named range data and location
    CC_ini_cell = [ws_aux.Range(range_name).Row, ws_aux.Range(range_name).Column]
    CC_data = ws_aux.Range(range_name).Value[1:] # Header "Personnel costs"/"Operating costs" is included in named range
    ## New CCs would be input CCs + "xxx" for rest of cells until complete CC range
    new_CC_data = CC_input + ["xxx" for i in range(len(CC_input), len(CC_data))]
    ## Format to tuple in columns for write in Excel
    new_CC_data = tuple([(new_CC_data[i],) for i in range(len(new_CC_data))])
    
    ## Write new Personnel costs CC in Excel 
    ws_aux.Range(colnum_string(CC_ini_cell[1]) + str(CC_ini_cell[0] + 1) + ":" + colnum_string(CC_ini_cell[1]) + str(CC_ini_cell[0] + len(new_CC_data))).Value = new_CC_data
    
    #### Unhide row
    ws_aux.Rows(str(CC_ini_cell[0] + 1) + ":" + str(CC_ini_cell[0] + len(CC_data))).EntireRow.Hidden = False

    ## Hide rows with "xxx"s
    if len(CC_input) < len(CC_data):
        ws_aux.Rows(str(CC_ini_cell[0] + 1 + len(CC_input)) + ":" + str(CC_ini_cell[0] + len(CC_data))).EntireRow.Hidden = True

    return CC_data

################################################################################
# Main function
################################################################################

def update_CC(wb_inp, ws_list, ws_aux, CC_pers_input, CC_op_input, client_name, n_prev_OC = 5):
    ## Update CCs in Aux ws
    ### CCs - Personnel costs
    CC_pers_data = update_CCs_in_Aux(ws_aux, "CC_Personnel", CC_pers_input)
    CC_op_data = update_CCs_in_Aux(ws_aux, "CC_Operating", CC_op_input)
    
    ## Hide CC rows with "xxx"s in the next worksheets: "OC", "PC", "PL"s and "P&L"s
    if min(len(CC_pers_input), len(CC_op_data)) < len(CC_pers_data):
        ## OC
        if "OC" in ws_list:
            hide_xxx_rows_OC(wb_inp, CC_op_input, "OC", "Operating costs", n_prev_OC)
            
        ## PC
        if "PC" in ws_list:
            hide_xxx_rows_PC(wb_inp, "PC", "Personnel cost")
    
        ## PL and P&L sheets
        PL_sheets = [ws_list[i] for i in range(len(ws_list)) if (ws_list[i] == "PL" or ws_list[i] == "P&L" or "PL-" in ws_list[i] or "P&L-" in ws_list[i])]
        if len(PL_sheets) > 0:
            for i in range(len(PL_sheets)):
                hide_xxx_rows_PL(wb_inp, PL_sheets[i], client_name)
                
    return

