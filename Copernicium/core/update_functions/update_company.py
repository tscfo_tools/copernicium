
from Copernicium.core.excel_functions import colnum_string


def update_companies(ws_aux, company_name, company_code, currency):
    
    # Get range "Companies" data and location
    comp_ini_cell = [ws_aux.Range("Companies").Row, ws_aux.Range("Companies").Column]
    comp_data = ws_aux.Range("Companies").Value
    n_comp = len(comp_data) - 2
    
    # Clear company data
    ws_aux.Range(colnum_string(comp_ini_cell[1]) + str(comp_ini_cell[0] + 1) + ":" + colnum_string(comp_ini_cell[1] + 2) + str(comp_ini_cell[0] + n_comp)).ClearContents()
    
    # Unhide rows for region "Companies"
    ws_aux.Rows(str(comp_ini_cell[0]) + ":" + str(comp_ini_cell[0] + (len(comp_data) - 1))).EntireRow.Hidden = False

    # If not enough rows in range "Companies", add rows. 
    if len(company_name) > n_comp:
        ws_aux.Range(str(comp_ini_cell[0] + n_comp) + ":" + str(comp_ini_cell[0] + len(company_name) - 1)).EntireRow.Insert()
    #If there more rows than companies, hide them
    elif len(company_name) < n_comp:
        ws_aux.Rows(str(comp_ini_cell[0] + len(company_name) + 1) + ":" + str(comp_ini_cell[0] + n_comp)).EntireRow.Hidden = True

    # Add new company data
    new_comp_data = tuple([(company_name[i], company_code[i], currency[i]) for i in range(len(company_name))])
    ws_aux.Range(colnum_string(comp_ini_cell[1]) + str(comp_ini_cell[0] + 1) + ":" + colnum_string(comp_ini_cell[1] + 2) + str(comp_ini_cell[0] + len(company_name))).Value = new_comp_data

    return
