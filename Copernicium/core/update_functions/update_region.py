
import win32com.client as win32
import xlwings as xl

from Copernicium.core.excel_functions import colnum_string


def update_region_data(wb, ws_aux, region):
    
    rename_sheets, ws_to_hide = [], []
    # KPIs
    ## Get range "Companies" data and location
    region_ini_cell = [ws_aux.Range("KPIs_Regions").Row, ws_aux.Range("KPIs_Regions").Column]
    region_data = ws_aux.Range("KPIs_Regions").Value
    
    ## Clear region col
    ws_aux.Range(colnum_string(region_ini_cell[1] + 1) + str(region_ini_cell[0] + 1) + ":" + colnum_string(region_ini_cell[1] + 1) + str(region_ini_cell[0] + 3)).ClearContents()
    
    ## Add region data
    if len(region) > 0:
        new_region_data = tuple([(region[i],) for i in range(len(region))])
        ws_aux.Range(colnum_string(region_ini_cell[1] + 1) + str(region_ini_cell[0] + 1) + ":" + colnum_string(region_ini_cell[1] + 1) + str(region_ini_cell[0] + len(region))).Value = new_region_data
    
    ##### Unhide rows in PL ranges
    ws_aux.Rows(str(region_ini_cell[0]) + ":" + str(region_ini_cell[0] + len(region_data) - 2)).EntireRow.Hidden = False

    ## Hide empty rows 
    if len(region) < 3:
        ws_aux.Rows(str(region_ini_cell[0] + len(region) + 1) + ":" + str(region_ini_cell[0] + len(region_data) - 2)).EntireRow.Hidden = True
        ## Define regions to hide
        regions_to_hide = [region_data[i][-1] for i in range(1 + len(region), len(region_data)-1)]
        ws_to_hide = ws_to_hide + ["KPIs-" + regions_to_hide[i] for i in range(len(regions_to_hide))] + ["Metrics-" + regions_to_hide[i] for i in range(len(regions_to_hide))]

    ## Sheets to rename and hide
    rename_sheets = rename_sheets + ["KPIs-" + region[i] for i in range(len(region))] + ["Metrics-" + region[i] for i in range(len(region))]
    
    
    
    # KPIs
    ## Get range "Companies" data and location
    region_ini_cell = [ws_aux.Range("PL_Regions").Row, ws_aux.Range("PL_Regions").Column]
    region_data = ws_aux.Range("PL_Regions").Value
    
    ## Clear region col except "HQ"
    ws_aux.Range(colnum_string(region_ini_cell[1] + 1) + str(region_ini_cell[0] + 2) + ":" + colnum_string(region_ini_cell[1] + 2) + str(region_ini_cell[0] + 4)).ClearContents()
    
    ## Add region data
    if len(region) > 0:
        new_region_data = tuple([(region[i],region[i]) for i in range(len(region))])
        ws_aux.Range(colnum_string(region_ini_cell[1] + 1) + str(region_ini_cell[0] + 2) + ":" + colnum_string(region_ini_cell[1] + 2) + str(region_ini_cell[0] + 1 + len(region))).Value = new_region_data
    
    ##### Unhide rows in PL ranges
    ws_aux.Rows(str(region_ini_cell[0]) + ":" + str(region_ini_cell[0] + len(region_data) - 2)).EntireRow.Hidden = False

    ## Hide empty rows 
    if len(region) < 3:
        ws_aux.Rows(str(region_ini_cell[0] + 1 + len(region) + 1) + ":" + str(region_ini_cell[0] + len(region_data) - 2)).EntireRow.Hidden = True
        ## Define regions to hide
        regions_to_hide = [region_data[i][-1] for i in range(2 + len(region), len(region_data)-1)]
        ws_to_hide = ws_to_hide + ["PL-" + regions_to_hide[i] for i in range(len(regions_to_hide))] + ["P&L-" + regions_to_hide[i] for i in range(len(regions_to_hide))]
    
    ## Sheets to rename (Add "HQ")
    rename_sheets = rename_sheets + ["PL-HQ"] + ["PL-" + region[i] for i in range(len(region))] + ["P&L-HQ"] + ["P&L-" + region[i] for i in range(len(region))]
    
    # Unhiding sheets if hidden
    for sheet_obj in wb.Sheets:
        if sheet_obj.Name[:5] == 'KPIs-' or sheet_obj.Name[:3] == 'PL-' or sheet_obj.Name[:8] == 'Metrics-' or sheet_obj.Name[:4] == 'P&L-':
            sheet_obj.Visible = win32.constants.xlSheetVisible    
    
    # Hiding region ws if necessary
    if len(ws_to_hide) > 0:
        for hiding_sheet in ws_to_hide:
            wb.Sheets(hiding_sheet).Visible = win32.constants.xlSheetHidden
    
    
    return


def modify_region_ws(excel, wb_inp, output_file_path, region):
    # Library "win32" is not able of changing ws name, but library "xlwings" does.
    ## Close wb open with win32 
    wb_inp.SaveAs(Filename= output_file_path)
    wb_inp.Close(True)
    excel.Visible = False 
    excel.DisplayAlerts = False
    excel.Application.Quit()
    
    
    ## Open wb with xlwings
    app = xl.App(visible = False)
    wb = app.books.open(output_file_path)    
    
    
    for ini_str in ["KPIs-", "Metrics-", "PL-", "P&L-"]:
        ind = 1
        for i in range(len(wb.sheets)):
            ## Rename sheets regions as "Region1, Region2, etc
            if ini_str in wb.sheets[i].name and "HQ" not in wb.sheets[i].name:
                wb.sheets[i].name = ini_str + "Region" + str(ind)
                ind += 1
        # Rename sheets with target region
        for i in range(len(region)):
            wb.sheets[ini_str + "Region" + str(i + 1)].name = ini_str + region[i]
                
    
    # Close wb opened with xlwings
    wb.save()
    wb.close()
    app.kill()
    
    return

