
from tool_info import *
from UI import app_management

def execute_Copernicium(tool_name):
    
    main_win = app_management.open_app(tool_name, tool_cover_link)
    # Wolfram responses
    app_management.actions_main_window(main_win)

          
if __name__=="__main__":

    # Execute tool
    execute_Copernicium(tool_name)